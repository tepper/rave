"""
    Author:
        Dr. Thor Tepper-García
    Created:
        27 MAR 2023
    About:
        Creates a dual filter to select particles with ID consistent with my Ramses particle ID.
        This step is necessary to be able to calculate the centre of mass of a snapshot relative to a component identified by its unique ID.

        Some YT documentation on particle filters is available at:

        https://yt-project.org/doc/analyzing/filtering.html?highlight=filters%20combined
"""
import yt

star_part_type = 2 # MAKE sure this identifies stars

def _filter_part_by_ID(ds, compID, MaxCompNumber):
    """
    Selects collisionless particles that are NOT stars (which are those with particle_type or particle_family = 2), and whose unique (custom) ID is related to the component ID such that:
        mod(part_ID, 2*MaxCompNumber) = compID
    """
    def _check_particles(ptype,alldata):
        mask = alldata[(ptype, "particle_identity")] % (2*MaxCompNumber) ==  compID
        num_part = mask.sum()
        if num_part < 1:
            raise ValueError(f'No particles in component {compID}')
        else:
            print(f'Found {num_part} particles with component ID {compID}')


    def _notstars(pfilter, data):
        filter = data[(pfilter.filtered_type, "particle_family")] == star_part_type
        return ~filter


    def _comp_filter(pfilter, data):
        filter = (data[(pfilter.filtered_type, "particle_identity")] % (2*MaxCompNumber) == compID)
        return filter


    # check for particles
    _check_particles('nbody', ds.all_data())

    yt.add_particle_filter("notstars", function=_notstars,
        filtered_type='nbody', requires=["particle_identity"])

    new_part_type_str = f"comp_{compID}"

    yt.add_particle_filter(new_part_type_str, function=_comp_filter,
        filtered_type='notstars', requires=["particle_family"])

    ds.add_particle_filter("notstars")
    ds.add_particle_filter(f"comp_{compID}")

    return new_part_type_str



if __name__ == '__main__':

    # for testing purposes only

    import numpy as np

    compID = 5
    MaxCompNumber = 11
    model = 'fgas10_agama_rf'
    snap = 201

    ds = yt.load(f"test_data/{model}/output_{snap:05d}")#"/info_{snap:05d}.txt")
    # print(ds.field_list)

    newpartstr = _filter_part_by_ID(ds, compID, MaxCompNumber)

    ad = ds.all_data()

    # expected output: one single value corresponding to the particle mass of the selected component:
    print(set(np.array(ad[(newpartstr, "particle_mass")])))

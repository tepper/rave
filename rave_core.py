'''
========================================================================
AUTHOR:
    Dr. Thor Tepper-Garcia

CREATED:
    14/04/2020
    -> In continuous development!

ABOUT:
    A class to provide a *customised* Pynbody approach to the analysis of
    N-body simulations. Intended mainly to be used in conjunction with a GUI
    interface, e.g. rave.py, but can also be used independently.

========================================================================
'''

# Import the necessary packages

# numerics
import numpy as np

import pynbody
from pynbody import units
from pynbody import transformation

# The following is actually only needed to create exectuable; it is needed
# because of module pynbody/gravity/calc.py makes use of it:
from pynbody import openmp

# required to read in arguments
import sys, ast

from collections import defaultdict

# required to monitor emory usage
import resource
import gc

# required to create 2D histogram
from scipy import stats

# Agama library
try:
    import agama
    use_agama = True
except:
    print('Could not import AGAMA module')
    use_agama = False

class Snapshot_Processing():

    def __init__(self,snap=None,maxcomp=None,complst=None,skip=None,
        verbose=False):

        if snap is None or maxcomp is None or complst is None or skip is None:
            raise ValueError('Snapshot_Processing: All input parameters are '+\
                'required to have valid values')

        self.snapfile=snap
        self.MaxCompNumber=maxcomp
        self.skip_part=skip
        self.verbose = verbose

        self.snap_object = None
        self.radius_ref = None
        self.compPart_all_skip = None
        self.comp_dict = defaultdict(list)
        self.grav_soft = 0.05 # arbitrary value

        # *total* number of particles after load
        self.num_part = 0

        # simulation time (only meaningful for one snapshot)
        self.timeMyr = 0.

        # very important if using the AGAMA library
        if use_agama:
            # units (in Msun, Kpc, km/s)
            agama.setUnits(mass=1, length=1, velocity=1)
            # careful with this:
            self.total_potential = None
            self.force = None
            self.force_derivs = None
            self.actions = None

        # Total potential requires to define a corresponding potential type
        # for each component.
        # The options are:
        #    Multipole ~ quasi-spherical mass distribution
        #    CylSpline ~ disc-like mass distribution
        # To avoid the need for yet another input parameter, the convention
        # adopted here is that positive component ID indicates a disky
        # component, and a negative, a spheroidal one
        self.potential_type = \
            ['Multipole' if id < 0 else 'CylSpline' for id in complst]
        # all component ID positive henceforth
        self.compID_list = [abs(id) for id in complst]


        # VERY IMPORTANT:
        # add new filters to pynbody.filt class
        pynbody.filt.Arc = self.Arc
        pynbody.filt.CircularSector = self.CircularSector
        pynbody.filt.QuantityFilter = self.QuantityFilter

        # load data
        self.loadsnap()


    # Functions and methods
    def stdout_info(self,str=None):
        '''
        Prints str to stdout in verbose mode
        '''
        if self.verbose: print(str)


    def _norm(self,vec):
        '''
        Returns Euclidean norm of vec
        '''
        return np.sum(vec ** 2) ** 0.5


    def memory(self):
        mem_MB = (resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)/1024/1024;
        self.stdout_info('\nMemory load (MB): %4d\n' % mem_MB)


    def loadsnap(self):
        '''
        Loads entire snapshot into object snapdata
        '''
        self.stdout_info('\nLoading snaphot:\n{}\n'.format(self.snapfile))
        self.stdout_info('\nYou may ignore the following Pynbody warning:\n')
        self.snap_object = pynbody.load(self.snapfile)
        # allow to load initial conditions (i.e. not yet output by Ramses)
        try:
            self.snap_object.set_units_system(distance='kpc',mass='Msol',
                velocity='km s^-1')
        except:
            pass
        self.snap_object.physical_units()
        self.stdout_info('\nDone loading snaphot.')
        self.timeMyr = self.snap_object.properties['time'].in_units('Myr')
        self.stdout_info('Simulation time: {} Myr'.format(self.timeMyr))
        # create array with softening length if non-existent
        self._create_new_key(self.snap_object,'eps',np.float64,self.grav_soft)
        # create array with level information if non-existent
        self._create_level()
        # create age array if not existent
        try:
            self._create_tform()
        except:
            print('Cannot create tform (likely because there are no stars)')
            print('Will ignore and move on')
        # create hydro variables (pressure, density) for NON-gaseous components
        self._create_hydrovars()
        # create particle ID array across families
        self._undo_lazyloading('mass') # needed if special families present
        self._undo_lazyloading('metals') # needed if stars present
        self._undo_lazyloading('phi') # needed for some reason...
        self._create_iords()


    def _undo_lazyloading(self,keyname=None):
        '''
        Call derivable arrays to overcome lazy loading
        '''
        if keyname is None:
            raise ValueError('Provide a key name')

        self.stdout_info('Overcome lazy loading for key {}...'.format(keyname))
        for fam_name in pynbody.family.family_names(): # loop over families
            fam = pynbody.family.get_family(fam_name)
            if len(self.snap_object[fam]) > 0:
                try:
                    self.snap_object[fam][keyname]
                except:
                    pass
        self.stdout_info('Done.')


    def _create_iords(self):
        '''
        See whether can be included into _create_new_key or, rather, use _create_new_key here
        Create uniform ID array for all particles
        E.g. sinks in Ramses are assignated 'id' which is read in as
        a float64 by Pynbody, complicating things a bit; thus:
        '''
        self.stdout_info('Creating iords...')
        # initialise iord array
        # for fam_name in pynbody.family.family_names(): # loop over families
        # Loop only over families actually in snapshot
        for fam_name in self.snap_object.families(): # loop over families
            fam = pynbody.family.get_family(fam_name)
            if 'iord' in self.snap_object[fam].all_keys():
                pass # iord already exists
            else:
                size = len(self.snap_object[fam])
                self.snap_object[fam]['iord'] = \
                    pynbody.array.SimArray( np.zeros(size),
                        dtype=np.int32) # must be int32
        # now fill with values
        # for fam_name in pynbody.family.family_names(): # loop over families
        # Loop only over families actually in snapshot
        for fam_name in self.snap_object.families(): # loop over families
            fam = pynbody.family.get_family(fam_name)
            if len(self.snap_object[fam]) > 0:
                #special cases: gas, stars
                if fam_name == 'gas':
                    compID_gas = 0
                    self.stdout_info(f'...for family {fam}')
                    size = len(self.snap_object[fam])
                    self.snap_object[fam]['iord'] = \
                        pynbody.array.SimArray( np.full(size, compID_gas),
                            dtype=np.int32) # must be int32
                elif fam_name == 'star':
                    compID_star = 2
                    self.stdout_info(f'...for family {fam}')
                    self.stdout_info(f'WARNING: Will OVERWRITE original iords!')
                    size = len(self.snap_object[fam])
                    self.snap_object[fam]['iord'] = \
                        pynbody.array.SimArray( np.full(size, compID_star),
                            dtype=np.int32) # must be int32
                else:
                    if 'iord' in self.snap_object[fam].all_keys():
                        pass # iords already exists
                    else: # usually black-hole particles
                        self.stdout_info(f'...for family {fam}')
                        self.snap_object[fam]['iord'] = \
                            pynbody.array.SimArray(\
                            np.int32(self.snap_object[fam]['id'])) # must be int32
            # without the following, the snap_object appears not to be updated:
            iords = self.snap_object['iord']
        self.stdout_info('Done.')


    def _create_hydrovars(self):
        '''
        A weird function: it creates a pressure and density array for *non-gaseous* components in order to make these available to actual gaseous components
        Also, it corrects for the mean molecular weight value (1.22) that Pynbody introduces when calculating the temperature (see ramses.py)
        '''
        gas = pynbody.family.get_family('gas')
        if len(self.snap_object[gas]) > 0:
            # self.stdout_info('Correcting temperature (pressure) for mean molecular weight (1.22)..')
            # # this is wrong, as the pressure is fine, but cannot overwrite 'temp' as it is a derived array
            # self.snap_object[gas]['p'] = self.snap_object[gas]['p'] / 1.22
            # the following is required in order to calle hydro-vars across the simulation:
            _hydroVars = ['p', 'rho', 'tracer']#, 'temp']
            self.stdout_info(f'Creating hydro vars {_hydroVars}...')
            for fam_name in pynbody.family.family_names(): # loop over families
                fam = pynbody.family.get_family(fam_name)
                if len(self.snap_object[fam]) > 0:
                    if fam_name != 'gas':
                        self.stdout_info('for family {}'.format(fam_name))
                        for _hydroVar in _hydroVars:
                            size = len(self.snap_object[fam])
                            self.snap_object[fam][_hydroVar] = \
                                pynbody.array.SimArray( np.zeros(size),
                                    dtype=np.float32)
                            # self.snap_object[fam]['rho'] = \
                            #     pynbody.array.SimArray( np.ones(size),
                            #         dtype=np.float32)
        return


    def _create_tform(self):
        '''
        Create tform (formation time) array for all particles that lack it
        '''
        self.stdout_info('Creating tform array...')
        for fam_name in pynbody.family.family_names(): # loop over families
            fam = pynbody.family.get_family(fam_name)
            if len(self.snap_object[fam]) > 0:
                try:
                    self.snap_object[fam]['tform']
                except:
                    self.stdout_info('...for family {}'.format(fam))
                    size = len(self.snap_object[fam])
                    self.snap_object[fam]['tform'] = \
                        pynbody.array.SimArray(np.zeros(size),
                            dtype=np.int32) # must be int32
        self._set_age_units()

        self.stdout_info('Done.')


    def _set_age_units(self):
        '''
        Convert default (and weird) age units to Myr *in place*
        '''
        self.snap_object['age'].convert_units('Myr')


    def _create_level(self):
        '''
        See whether can be included into _create_new_key or, rather, use _create_new_key here
        Create uniform level array for all particles that lack it
        '''
        self.stdout_info('Creating level array...')
        for fam_name in pynbody.family.family_names(): # loop over families
            fam = pynbody.family.get_family(fam_name)
            if len(self.snap_object[fam]) > 0:
                try:
                    self.snap_object[fam]['level']
                except:
                    self.stdout_info('...for family {}'.format(fam))
                    size = len(self.snap_object[fam])
                    self.snap_object[fam]['level'] = \
                        pynbody.array.SimArray(np.zeros(size),
                            dtype=np.int32) # must be int32
                    # set level to minimum refinement level
                    try:
                        self.snap_object[fam]['level'] = self.snap_object._info['levelmin']
                    except: # likely not a ramses file
                        self.snap_object[fam]['level'] = 7 # arbitrary

        self.stdout_info('Done.')


    def _create_new_key(self,snapobj=None,keyname=None,dtype=None,cont=None):
        '''
        Create a new 'derivable' key (i.e. sim array) for snapobj and populate
        it with cont of type dtype
        Requires the existence of a function that calculates the key
        '''
        if snapobj is None:
            raise ValueError('Provide a snapshot object')
        if keyname is None:
            raise ValueError('Provide a key name')
        if dtype is None:
            raise ValueError('Provide a data type (e.g. np.float64)')
        if cont is None:
            raise ValueError('Provide a content (may be a function with args)')

        self.stdout_info('Creating new {} array...'.format(keyname))
        # see if it exists already
        try:
            snapobj[keyname]
        except:
            _array_size = len(snapobj)
            snapobj[keyname] = \
                pynbody.array.SimArray(np.empty(_array_size,dtype=dtype))
            # populate content
            try:
                snapobj[keyname] = cont
            except:
                raise ValueError('Could not create new key \'{}\''.\
                    format(keyname))

        self.stdout_info('Done.')


    def _select_subset(self,compID=None,mask=None,maskall=None):
        '''
        Select a sub-set of particles for given component
        '''
        _part_num = np.sum(mask == True)         # count particles (True values)
        if self.skip_part > 1 and _part_num > 1: # go ahead only if necessary
            _true_indx = np.where(mask)[0]         # retrieve indices where True
            _true_indx = \
                list(_true_indx)[::self.skip_part] # create shorter list
            mask[1:] = np.full(len(mask)-1,False)  # reset mask (skip first)
            mask[ _true_indx ] = True               # update mask at appr. ind.

        maskall = maskall | mask                  # bitwise update maskall

        self.comp_dict[compID] = self.snap_object[mask]
        # ensure (near) mass conservation
        # avoid skip_part = 1 as it would try to copy an array onto itself!
        # IMPORTANT: DO NOT APPLY THE FOLLOWING TO GAS (because gas cells do not have the same mass)
        if self.skip_part > 1 and len(self.comp_dict[compID]) > self.skip_part:
            self.snap_object['mass'][mask] = \
                self.skip_part * self.snap_object['mass'][mask]

        return maskall


    def select_particles(self):
        '''
        Stores each component as a list of particles identified by its ID
        into a dict
        IMPORTANT: Consider calling this by default
        '''
        return_message = ''

        # note: all components should have at least one particle
        # otherwise an error is raised
        _mask_all = np.full(len(self.snap_object),False)

        self.stdout_info('\nSelecting every {:} particle of ...'.format(self.skip_part))
        for _compID in self.compID_list:
            self.stdout_info('component {}'.format(_compID))
            # TEMPORARY AND HORRIBLE HACK to allow loading other simulations which do not follow my ID convention, and when distinguishing between true gas particles (ID = 0) and other particles is needed (added 29 JAN 2023 and to be removed SOON!!!)
            # IMPORTANT: will fail when loading gas + other particles
            if _compID == 0: # true gas
                _mask = \
                    np.array(self.snap_object['iord'] == _compID)

            elif _compID == 2: # true stars
                _mask = \
                    np.array(self.snap_object['iord'] == _compID)

            else: # everything else collisionless (DM, pre-existing 'stars')
                _mask = \
                    np.array(self.snap_object['iord'] % \
                    (2*self.MaxCompNumber) == _compID)

            if np.all(_mask == False) == True:
                message = f'No particles found in component {_compID}'
                raise ValueError(message)
            else: # select a sub-set of particles
                _mask_all = \
                    self._select_subset(_compID,_mask,_mask_all)

        self.stdout_info('Done.\n')
        # select all required particles (taking into account skip_part)
        self.compPart_all_skip = self.snap_object[_mask_all]
        self.num_part = len(self.compPart_all_skip)
        message = f'Selected (with skip) number of particles: {self.num_part}'
        self.stdout_info(message)

        # free memory
        del _mask_all, _mask
        gc.collect()
        return


    def com(self,compID=None):
        '''
        Calculates and shifts system to centre of mass of given component
        Assumes uniform particle mass
        Returns final CoM coordinates
        '''
        if compID is None:
            raise ValueError('Provide a component ID')
        self.stdout_info('\nShifting system to COM of component {}...'.\
            format(compID))

        # First iteration
        iter=1
        try:
            _posarray = self.comp_dict[compID]['pos']
        except KeyError:
            self._undo_lazyloading('pos')
            _posarray = self.comp_dict[compID]['pos']

        _part_selection = self.comp_dict[compID]

        _CoM = pynbody.analysis.halo.center_of_mass(_part_selection)
        _CoM_init = _CoM # save for return value

        self.stdout_info('Initial CoM: {}'.format(_CoM_init))

        # CAUTION: this overwrites the original data!
        transformation.translate(self.snap_object,-_CoM)

        # Iterate until convergence
        _r_inner0 = float((self.comp_dict[compID]['r']).mean())
        _r_inner  = _r_inner0
        self.radius_ref = _r_inner
        _r_toler  = self.grav_soft # full arbitrary

        # if compID == 0: # no further iterations for gas components (why?)
        #     _CoM = pynbody.analysis.halo.center_of_mass(_part_selection)
        #     self.stdout_info('New CoM: {}'.format(_CoM))
        #     return _CoM

        if len(self.comp_dict[compID]) > 1:

            while True:

                self.stdout_info('Iteration: {}'.format(iter))
                self.stdout_info('r_inner: {}'.format(_r_inner))
                self.stdout_info('r_toler: {}'.format(_r_toler))

                # Create a filter to select the appropriate particles
                # within a given radius
                _part_selection = \
                    self.comp_dict[compID][pynbody.filt.Sphere(self.radius_ref)]
                _CoM = pynbody.analysis.halo.center_of_mass(_part_selection)

                self.stdout_info('New CoM: {}'.format(_CoM))
                _CoMd = np.linalg.norm(_CoM)
                if _CoMd < _r_toler or _r_inner < _r_toler:    # CoMd small enough
                     break
                iter += 1
                transformation.translate(self.snap_object,-_CoM)
                _r_inner = 0.5 * _r_inner        # reduce search radius

            self.radius_ref = _r_inner

            # free memory
            del _part_selection,_CoMd,_posarray

        self.stdout_info('Done.\n')

        # free memory
        gc.collect()
        return _CoM_init #_CoM


    def cov(self,compID=None):
        '''
        Calculates and shifts system to centre of velocity of given component
        Assumes uniform particle mass
        Requires self.radius_red and thus depends on calling com first
        Returns final CoV coordinates
        '''
        if compID is None:
            raise ValueError('Provide a component ID')

        if self.radius_ref is None:
            raise ValueError('r_inner not available; call \'com\' first.')

        self.stdout_info('\nShifting system to COV of component {}...'.\
            format(compID))

        if len(self.comp_dict[compID]) > 1:
            _part_selection = \
                self.comp_dict[compID][pynbody.filt.Sphere(self.radius_ref)]
        else:
            _part_selection = self.comp_dict[compID]

        try:
            _velarray = _part_selection['vel']
        except KeyError:
            self._undo_lazyloading('vel')
            _velarray = _part_selection['vel']

        _CoV = pynbody.analysis.halo.center_of_mass_velocity(_part_selection)
        _CoV_init = _CoV # save for return value

        self.stdout_info('Initial CoV: {}'.format(_CoV_init))
        transformation.v_translate(self.snap_object,-_CoV)
        _CoV = pynbody.analysis.halo.center_of_mass_velocity(_part_selection)
        self.stdout_info('New CoV: {}'.format(_CoV))

        self.stdout_info('Done.\n')

        # free memory
        del _part_selection,_velarray
        gc.collect()
        return _CoV_init #_CoV


    def alignangmom(self,compID=None):
        '''
        Aligns angular momentum vector of given component with +z-axis
        '''
        if len(self.comp_dict[compID]) < 2:
            return
        if compID is None:
            raise ValueError('Provide a component ID')
        if self.radius_ref is None:
            raise ValueError('r_inner not available; call \'com\' first.')

        self.stdout_info(\
        '\nRotating system to align Lvec of component {} with +z-axis...'.\
        format(compID))
        self.stdout_info('r_inner = {}'.format(self.radius_ref))
        _Lvec = \
            pynbody.analysis.angmom.ang_mom_vec(
                self.comp_dict[compID][pynbody.filt.Sphere(self.radius_ref)]
            )
        _normal = _Lvec / self._norm(_Lvec)
        self.stdout_info('Initial L_norm = {}'.format(_normal))
        # rotate Lvec onto +z-axis (right-hand coordinate system)
        transformation.transform(self.snap_object,
            pynbody.analysis.angmom.calc_faceon_matrix(_normal))
        # check
        if self.verbose:
            _Lvec = \
                pynbody.analysis.angmom.ang_mom_vec(
                    self.comp_dict[compID][
                        pynbody.filt.Sphere(self.radius_ref)]
                )
            _normal = _Lvec / self._norm(_Lvec)
            self.stdout_info('Final L_norm = {}'.format(_normal))
        self.stdout_info('Done.\n')

        # free memory
        del _Lvec, _normal
        gc.collect()

    def profile_Toomre_Q_gas(self,minR,maxR,resol,pltrng,scale=None):
        '''
        Calculates the radial profile of Toomre's Q stability parameter of a gas disk
        Note: Pynbody's inbuilt toomre_q is bugged.
        Assumes the first item in compID_list is the disc component.
        But the others are required to calculate the total potential and derived
        quantities (total circular velocity, epicyclic frequency)
        Returns radial bins, 1D profile
        '''
        # TTG: BE AWARE OF UNITS AND ADOPTED VALUES HERE AND IN AGAMA/RAMSES+GADGET
        # gas relevant parameters
        Xh = 0.76 # hydrogen mass fraction
        kb = 1.3806490e-23 #J/K -> identical to RAMSES
        mp = 1.6726e-27 #kg -> RAMSES uses AMU = 1.6605390d-27 kg
        mu_mol = 1.0/(Xh/1.0+(1.0-Xh)/4.0) # Molecular weight per ion  -> identical to RAMSES
        gamma = 5.0/3.0 # adiabatic constant
        # Gravitational constant in astrophysical units
        Grav = 6.67300e-11 * (units.m.ratio(units.kpc)) * \
        (units.m.ratio(units.km))**2 * \
            (units.kg.ratio(units.Msol))**(-1) #* units.s**(-2)
        # requires an array with the softening length
        _points=resol
        if scale is None:
            scale='log' # options: 'lin', 'log' -> bin size in log/lin space
        # profile of disc
        pcyl_comp = pynbody.analysis.profile.Profile(\
            self.comp_dict[self.compID_list[0]],min=minR,
            max=maxR,nbins=resol,type=scale,ndim=2)
        R_bin = pcyl_comp['rbins']         # requires 'scale' to be equal in vc_tot!
        # surface density of disc
        Sigma_bin = pcyl_comp['density']
        # temperature (K) profile of disc and sound speed (km/s)
        T_bin = pcyl_comp['temp']
        cs_bin =  np.sqrt(kb * T_bin / (mu_mol * mp)) * 1.e-3
        # total circular velocity
        pcyl = pynbody.analysis.profile.Profile(\
            self.compPart_all_skip,min=minR,
            max=maxR,nbins=resol,type=scale,ndim=2)
        vc_tot = pcyl['v_circ']
        # angular frequency
        omega_bin = vc_tot / R_bin
        # epicyclic (radial) frequency
        # k^2 = R dOmega^2 / dR + 4 Omega^2 = 2R Omega dOmega / dR + 4 Omega^2
        dOmega2dR = 2*omega_bin*np.gradient(omega_bin)/np.gradient(R_bin)
        kappa_bin = np.sqrt(R_bin * dOmega2dR + 4 * omega_bin**2)
        # Toomre's Q (dimensionless)
        toomre_bin = cs_bin * kappa_bin / (np.pi * Grav * Sigma_bin )
        return R_bin, toomre_bin


    def profile_Toomre_Q_star(self,minR,maxR,resol,pltrng,scale=None):
        '''
        Calculates the radial profile of Toomre's Q stability parameter of a stellar disk
        Note: Pynbody's inbuilt toomre_q is bugged.
        Assumes the first item in compID_list is the disc component.
        But the others are required to calculate the total potential and derived
        quantities (total circular velocity, epicyclic frequency)
        Returns radial bins, 1D profile
        '''
        # Gravitational constant in astrophysical units
        Grav = 6.67300e-11 * (units.m.ratio(units.kpc)) * \
        (units.m.ratio(units.km))**2 * \
            (units.kg.ratio(units.Msol))**(-1) #* units.s**(-2)
        # requires an array with the softening length
        _points=resol
        if scale is None:
            scale='log' # options: 'lin', 'log' -> bin size in log/lin space
        # profile of disc
        pcyl_comp = pynbody.analysis.profile.Profile(\
            self.comp_dict[self.compID_list[0]],min=minR,
            max=maxR,nbins=resol,type=scale,ndim=2)
        R_bin = pcyl_comp['rbins']         # requires 'scale' to be equal in vc_tot!
        # surface density of disc
        Sigma_bin = pcyl_comp['density']
        # cylindrical radial velocity dispersion of disc
        sigma_vR_bin = pcyl_comp['vrxy_disp']
        # total circular velocity
        pcyl = pynbody.analysis.profile.Profile(\
            self.compPart_all_skip,min=minR,
            max=maxR,nbins=resol,type=scale,ndim=2)
        vc_tot = pcyl['v_circ']
        # angular frequency
        omega_bin = vc_tot / R_bin
        # epicyclic (radial) frequency
        # k^2 = R dOmega^2 / dR + 4 Omega^2 = 2R Omega dOmega / dR + 4 Omega^2
        dOmega2dR = 2*omega_bin*np.gradient(omega_bin)/np.gradient(R_bin)
        kappa_bin = np.sqrt(R_bin * dOmega2dR + 4 * omega_bin**2)
        # Toomre's Q (dimensionless)
        toomre_bin = sigma_vR_bin * kappa_bin / (3.36 * Grav * Sigma_bin )
        return R_bin, toomre_bin


    def profile(self,var,resol,pltrng,scale,dim,minR=None):
        '''
        Calculates a 1D axisymmetrically (dim=2) or spherically (dim=3) averaged density projection of the particle
        distribution weighted by mass
        All input parameters required
        Returns radial bins, 1D profile
        '''
        if minR is None:
            if var == 'v_circ': # why?
                _minR = 0.1
            else:
                _minR=self.grav_soft
        else:
            _minR = minR if minR > 0.1 else 0.1
        _maxR=pltrng
        # special cases; not (correctly) implemented in Pynbody
        if var == "Toomre_Q_star":
            prof = {}
            prof['rbins'], prof[var] = \
                self.profile_Toomre_Q_star(_minR,_maxR,resol,pltrng,scale)
        elif var == "Toomre_Q_gas":
            prof = {}
            prof['rbins'], prof[var] = \
                self.profile_Toomre_Q_gas(_minR,_maxR,resol,pltrng,scale)
        elif var == "Omega_z": # vertical frequency a.k.a. nu
            prof = {}
            prof['rbins'], prof[var] = \
                self.profile_Omega_z(_minR,_maxR,resol,pltrng,scale)
        else:
            prof = pynbody.analysis.profile.Profile(self.compPart_all_skip,
                min=_minR,max=_maxR,nbins=resol,type=scale,ndim=dim)

        return prof['rbins'], prof[var]


    def generic_weighted_map(self,x,y,w,resol,xmin,xmax,ymin,ymax,stat=None):
        '''
        Calculates a 2D projection of the particle distribution on the
        xy-plane, weighted by w.
        - The value of each 2D bin is determined by stat:
        'mean','std','median','count','sum','min','max'
        - x,y,w can be 1D array quantity, all of the same size
        Note: 'median' is very inefficient!
        Returns an object containing:
        - statistic(nx, ny) ndarray
            The values of the selected statistic in each two-dimensional bin.
        - x_edge(nx + 1) ndarray
            The bin edges along the first dimension.
        - y_edge(ny + 1) ndarray
            The bin edges along the second dimension.
        - binnumber(N,) array of ints or (2,N) ndarray of ints
        '''
        _range_x = [xmin,xmax]
        _range_y = [ymin,ymax]
        if stat is None:
            stat='sum'
        if isinstance(resol, type(1)):
            resol = [resol]
        if isinstance(resol, type(1.)):
            resol = [int(resol)]
        resol = [r for r in resol]
        if len(resol) == 1:
            bins = resol[0]
        elif len(resol) == 2: # allow for different resolutions along each axis
            bins = (resol[1], resol[0])
        else:
            raise ValueError(f'generic_weighted_map: parameter "resol" must be a scalar or a 2-element list / tuple but got {resol}')
        # note the reverse (!) order of coordinates
        counts_xy = \
            stats.binned_statistic_2d(y, x, w,
            statistic=stat, bins=bins, range=(_range_y,_range_x))
        # need to normalise by pixel size to guarantee consistency with varying resolution:
        if stat == 'sum':
            if len(resol) == 2: # is resol a list (assumed len = 2)
                pixel_size = (xmax-xmin)*(ymax-ymin)/resol[0]/resol[1]
            else:
                pixel_size = (xmax-xmin)*(ymax-ymin)/bins/bins
            # cannot overwrite attributes, so need to create a new instance:
            result = stats._binned_statistic.BinnedStatistic2dResult( statistic=counts_xy.statistic / pixel_size,  x_edge=counts_xy.x_edge, y_edge=counts_xy.y_edge,  binnumber=counts_xy.binnumber)
        else:
            result = counts_xy
        return result


    def potential(self,compID=None,typ=None):
        '''
        Calculates an interpolated version of the potential
        It is very important to provide the correct type:
            Multipole ~ quasi-spherical mass distribution
            CylSpline ~ disc-like mass distribution
        See self.potential_type above
        Returns an AGAMA potential instance 'pot'
        E.g. potential at (0,0,0) kpc is pot.potential(0,0,0))) in (km/s)^2
        Requires the AGAMA library
        '''
        if compID is None:
            raise ValueError('Provide a component ID')
        if typ is None:
            raise ValueError('Provide a type (Multipole/CylSpline)')
        if not use_agama:
            raise ValueError('AGAMA module not available')

        # Create an axisymmetric potential from the N-body model
        # symmetry = 'a'xisymmetric
        # symmetry = 't'riaxial -> default
        # symmetry = 's'pherical
        # IMPORTANT: The choice of lmax and other parameters (here taking their default value, e.g. rmin/max, zmin/max, mmax) may not be appropriate in general!

        # pot = agama.Potential( \
        #     type=typ, particles=(self.comp_dict[compID]['pos'], \
        #     self.comp_dict[compID]['mass']), \
        #     symmetry='a', gridsizeR=20, lmax=2)
        # The following was added on 30 NOV 2021 (takes too long...)
        # pot = agama.Potential( type=typ, symmetry='a',
        #         particles = (self.comp_dict[compID]['pos'],
        #             self.comp_dict[compID]['mass'])
        #     )

        if typ == 'CylSpline':
            # mmax=0 <-> symmetry = 'a'xisymmetric
            pot = agama.Potential(
                    type=typ,
                    particles=(self.comp_dict[compID]['pos'],
                    self.comp_dict[compID]['mass']),
                    mmax=0
                )
        if typ == 'Multipole':
            # symmetry='a' -> required by Rperiapo!
            pot = agama.Potential(
                    type=typ,
                    symmetry='a',
                    particles=(self.comp_dict[compID]['pos'],
                    self.comp_dict[compID]['mass']),
                    lmax=4
                )

        return pot


    def potential_tot(self):
        '''
        Calculates the total potential induced by a number of components
        identified by the items in input component list.
        Returns an AGAMA potential instance 'total_pot'
        E.g. total potential at (0,0,0) kpc is given by
        total_pot.potential(0,0,0))) in (km/s)^2
        Requires the AGAMA library
        '''
        if not use_agama:
            raise ValueError('AGAMA module not available')

        if self.total_potential is None:
            # calculate each component's potential
            comp_pot = []
            for compID, typ in zip(self.compID_list,self.potential_type):
                _pot = self.potential(compID,typ)
                comp_pot.append(_pot)
                del _pot
            # combine the individual potentials into a single composite one
            # WARNING: The following assumes no new snapshot has been loaded
            # NEED to trap this potential error
            total_pot  = agama.Potential(*comp_pot)
        else:
            total_pot = self.total_potential
        gc.collect()
        return total_pot


    def force_derivs_calc(self,coords=None):
        '''
        Convenience method
        Returs the x,y,z components of force per unit mass F, and the matrix of force derivatives stored as DF = dFx/dx,dFy/dy,dFz/dz,dFx/dy,dFy/dz,dFz/dx evaluated at coords. 'coords' must be at least a triplet, or an array thereof, expressed in kpc. If 'coords' is array of N triplets,
        then both F and DF are arrays with sizes Nx3 and Nx6, respectively.
        This method could be avoided by calling directly AGAMA's method .forceDeriv, but this would fail if the total potential does not exist yet. The way it's done here is fail safe.
        Requires the AGAMA library
        '''
        if not use_agama:
            raise ValueError('AGAMA module not available')

        if coords is None:
            raise ValueError('Provide at least a triplet of coordinates')
        total_pot = self.potential_tot()
        return total_pot.forceDeriv(coords)

    def psi(self):
        '''
        Convenience method
        Creates a new array containing the the gravitational potential induced by all loaded components (taking into account only non-skiped particles)
        Requires the AGAMA library
        RETURN VALUE: None
        '''
        if not use_agama:
            raise ValueError('AGAMA module not available')

        try:
            self.compPart_all_skip['psi']
        except:
            _coords = self.compPart_all_skip['pos']
            _pot = self.potential_tot()
            _pot_eval = _pot.potential(_coords)
            self._create_new_key(self.compPart_all_skip, 'psi',
                np.float32, _pot_eval)
        return

    def Omega_z(self):
        '''
        Convenience method
        Creates a new array containing the vertical oscillatory
        frequency, Omega_z in km/s/kpc, calculated according to:

            Omega_z^2 = d^2 (potential) / dz^2 = d (force_z) / dz

        Only for the component identified by the first entry in comp_list
        Requires the AGAMA library
        '''
        if not use_agama:
            raise ValueError('AGAMA module not available')

        try:
            self.comp_dict[self.compID_list[0]]['Omega_z']
        except:
            if self.force is None and self.force_derivs is None:
                self.force, self.force_derivs = \
                    self.force_derivs_calc(
                        self.comp_dict[self.compID_list[0]]['pos']
                    )
            # handle 1x3-array or Nx3-array cases:
            try:
                Omegaz2 = -1.*self.force_derivs[:,2]
            except:
                Omegaz2 = -1.*self.force_derivs[2]

            # remove negative entries; set to 0 to avoid error
            Omegaz2[np.where(Omegaz2 < 0)] = 0. # or np.nan
            Omegaz = np.sqrt(Omegaz2)
            # units from (km/s)/kpc -> Myr^-1
#             Omegaz = Omegaz*(units.km.ratio(units.kpc)/units.s.ratio(units.Myr))
            self._create_new_key(self.comp_dict[self.compID_list[0]],
                'Omega_z',np.float32,Omegaz)
            del Omegaz2,Omegaz
            gc.collect()
        return


    def actions_calc(self):
        '''
        Calculates actions (J_R, J_z, J_phi) for the component identified by
        the first entry in comp_list
        Requires the AGAMA library
        RETURN VALUE: array
            J_R = actions[:,0]
            J_z = actions[:,1]
            J_phi = actions[:,2]
        '''
        if not use_agama:
            raise ValueError('AGAMA module not available')

        if self.actions is None:
            _act_finder = agama.ActionFinder(self.potential_tot())
            self.actions = \
                _act_finder(
                    np.hstack((self.comp_dict[self.compID_list[0]]['pos'],
                    self.comp_dict[self.compID_list[0]]['vel']))
                )
        return self.actions


    def J_R(self):
        '''
        Convenience method
        Creates a new array containing the radial action (J_R) for the
        component identified by the first entry in comp_list, if not existent
        Requires the AGAMA library
        RETURN VALUE: None
        '''
        if not use_agama:
            raise ValueError('AGAMA module not available')

        try:
            self.comp_dict[self.compID_list[0]]['J_R']
        except:
            self._create_new_key(self.comp_dict[self.compID_list[0]],'J_R',
                np.float32,self.actions_calc()[:,0])
        return


    def J_z(self):
        '''
        Convenience method
        Creates a new array containing the vertical action (J_z) for the
        component identified by the first entry in comp_list, if not existent
        Requires the AGAMA library
        RETURN VALUE: None
        '''
        if not use_agama:
            raise ValueError('AGAMA module not available')

        try:
            self.comp_dict[self.compID_list[0]]['J_z']
        except:
            self._create_new_key(self.comp_dict[self.compID_list[0]],'J_z',
                np.float32,self.actions_calc()[:,1])
        return


    def J_phi(self):
        '''
        Convenience method
        Creates a new array containing the azimuthal action (J_phi) for the
        component identified by the first entry in comp_list, if not existent
        Requires the AGAMA library
        RETURN VALUE: None
        '''
        if not use_agama:
            raise ValueError('AGAMA module not available')

        try:
            self.comp_dict[self.compID_list[0]]['J_phi']
        except:
            self._create_new_key(self.comp_dict[self.compID_list[0]],'J_phi',
                np.float32,self.actions_calc()[:,2])
        return


    def energy(self):
        '''
        Specific energy calculated for each particle of the component
        identified by the first entry in comp_list
        Requires the AGAMA library
        RETURN VALUE: None
        '''
        if not use_agama:
            raise ValueError('AGAMA module not available')

        total_pot = self.potential_tot()
        try:
            self.comp_dict[self.compID_list[0]]['energy']
        except:
            energy = \
              total_pot.potential(self.comp_dict[self.compID_list[0]]['pos']) +\
              0.5 * np.sum(self.comp_dict[self.compID_list[0]]['vel']**2,
              axis=1)
            self._create_new_key(self.comp_dict[self.compID_list[0]],'energy',
                np.float32,energy)
            del energy
            gc.collect()
        return


    def e_Jacobi(self,omega_p):
        '''
        Calculate (a proxy for) the Jacobi Integral

        e_J = e_tot - omega_p * J_phi

        for the component identified by the first entry in comp_list.
        Here:

        e_tot := total specific energy
        Lz := z-component of ang. mom. (= J_phi := azimuthal action)
        omega_p := angular pattern speed (constant)

        Requires the AGAMA library.

        RETURN VALUE: None
        '''
        if not use_agama:
            raise ValueError('AGAMA module not available')

        print(omega_p)
        # try:
        #     self.comp_dict[self.compID_list[0]]['e_Jacobi']
        # except:

        # always compute (since omega_p may have changed -> improve!)
        # self.J_phi() # calculare J_phi
        # J_phi = self.comp_dict[self.compID_list[0]]['J_phi']
        # use Pynbody jz = Lz (faster than calculating J_phi)
        Lz = self.comp_dict[self.compID_list[0]]['jz']
        Lz = Lz.view(np.ndarray) # get rid of SubSnap wrapping (e.g. units)
        self.energy() # calculate total specific energy
        e_tot = self.comp_dict[self.compID_list[0]]['energy']
        # e_j = e_tot - omega_p * J_phi
        e_j = e_tot - omega_p * Lz
        self._create_new_key(self.comp_dict[self.compID_list[0]],'e_Jacobi',
            np.float32,e_j)
        return


    # Pynbody additions

    # variables not explicitly calculated by Pynbody
    def profile_Omega_z(self,minR,maxR,resol,pltrng,scale):
        '''
        Calculates the radial profile of the vertical frequency Omega_z (or nu)
        Assumes the first item in compID_list is the disc component.
        But the others are required to calculate the total potential and derived
        quantities (total circular velocity, epicyclic frequency)
        Returns radial bins, 1D profile
        '''
        try:
            self.compPart_all_skip['Omega_z']
        except:
            self.Omega_z()
        if scale == 'log':
            _bin_edges = \
                np.logspace(np.log10(minR),np.log10(maxR),num=resol+1)
        else: #scale == 'lin':
            _bin_edges = \
                np.linspace(minR,maxR,num=resol+1)
        hist1D = stats.binned_statistic(
            self.comp_dict[self.compID_list[0]]['rxy'],
            self.comp_dict[self.compID_list[0]]['Omega_z'],
            statistic='mean',
            bins=_bin_edges,range=(minR,maxR))
        # shift to bin centre
        rbins = \
            0.5*(hist1D.bin_edges[:-1] + hist1D.bin_edges[1:])
        return rbins, hist1D.statistic


    def omega(self):
        '''
        Angular frequency: omega = | v_phi / R |
        It is always positive regardless of the sign of v_phi!
        Note that this quantity *is* available per default in Pynbody but only as a profile; also, Pynbody uses v_circ rather than vphi
        RETURN VALUE: none
        '''
        try:
            self.snap_object['omega']
        except:
            self.stdout_info('Creating angular frequency array (vphi/rxy)')
            vphi = self.snap_object['vphi']
            R = self.snap_object['rxy']
            omega = abs(vphi / R)
            self._create_new_key(self.snap_object,'omega',np.float32,omega)
            del omega, vphi, R
            gc.collect()
            self.stdout_info('Done.')
        return


    # geometric filters (must be explicitly added to pybody.filt class; see __init__)
    def CircularSector(self,radius,height,anglemin,anglemax,cen=(0,0,0)):
        '''
        Convenience function that returns a filter which selects particles
        in a circular sector of radius (kpc) centered on cen (3-tuple, kpc),
        spanning a planar angle between anglemin and anglemax (deg)
        RETURN VALUE: filter instance
        '''
        pi = np.pi
        twopi = 2*pi
        # calculate azimuthal angle on xy-plane with range in [0,2Pi)
        # centred on cen (cf. Pynbody's 'az')
        self.snap_object['phi'] = \
            (np.arctan2(self.snap_object['y']-cen[1],
            self.snap_object['x']-cen[0])+twopi)%(twopi)
        # rad -> deg
        self.snap_object['phi'] = (180./pi)*self.snap_object['phi']
        f1 = pynbody.filt.BandPass('phi',anglemin,anglemax)
        f2 = pynbody.filt.Disc(radius,height,cen)
        return f1 & f2


    def Arc(self,r1,r2,height,anglemin,anglemax,cen=(0,0,0)):
        '''
        Convenience function that returns a filter which selects particles
        in between two circular sectors of radii r1 and r2 (kpc) centered
        on cen (3-tuple, kpc), spanning a planar angle between anglemin and
        angelmax (deg)
        RETURN VALUE: filter instance
        '''
        pi = np.pi
        twopi = 2*pi
        # calculate azimuthal angle on xy-plane with range in [0,2Pi)
        # centred on cen (cf. Pynbody's 'az')
        self.snap_object['phi'] = \
            (np.arctan2(self.snap_object['y']-cen[1],
            self.snap_object['x']-cen[0])+twopi)%(twopi)
        # rad -> deg
        self.snap_object['phi'] = (180./pi)*self.snap_object['phi']
        f1 = pynbody.filt.BandPass('phi',anglemin,anglemax)
        f2 = pynbody.filt.SolarNeighborhood(r1,r2,height,cen)
        return f1 & f2


    # quantity-based filter (must be explicitly added to pybody.filt class; see __init__)
    def QuantityFilter(self,quantity,min,max):
        '''
        Convenience function that returns a filter which selects particles
        with 'quantity' with values between min and max
        RETURN VALUE: filter instance
        '''
        # Special cases (not built in Pynbody; some calculated with AGAMA)
        _special_vars = \
            ('J_R','J_z','J_phi','Omega_z','energy','omega','e_Jacobi','psi')
        if any(q == quantity for q in _special_vars):
            try:
                self.comp_dict[self.compID_list[0]][quantity]
            except:
                getattr(self.snap_object,quantity)()
        f1 = pynbody.filt.BandPass(quantity,min,max)
        return f1

    # HERE: DEFINED NEW DERIVED ARRAYS USING PYNBODY'S METHODS
    @pynbody.snapshot.ramses.RamsesSnap.derived_quantity
    def metals(snap):
        """
        metal mass fraction derived from O and Fe with correction for all other metals; see Agertz et al. (2020), their equation 2 in natural units, i.e. not relative to solar. To get value relative to solar simply divide result by Zsun.
        IMPORTANT: Must pass 'snap' as argument as this is Pynbody's way!
        """
        oxy = snap['metal_O']
        fe= snap['metal_Fe']
        return (2.09*oxy + 1.06*fe)

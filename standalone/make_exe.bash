# Calls pre-make_exe.bash and redirects output to stdout and logfile
# NOTE that pre-make_exe.bash was called make_exe.bash prior to 12 MAR 2024!

exec ./pre-make_exe.bash 2>&1 | tee filebuild.log

#!/bin/bash

name='rave'

command='./clean.bash'
echo -e '\nIssuing command:\n'${command}
${command}

# add data (see * below)
# add pynbody data
# the following automatically retrieves the location, but may fail (make sure to USE THE APPROPRIATE VERSION OF PIP)
#src=`pip-3.9 show pynbody | grep Location | awk '{print $2}'`
# src=${src}'/pynbody/default_config.ini'
src='/Users/tepper/Library/Python/3.9/lib/python/site-packages/pynbody/default_config.ini'
dest='pynbody'
# NOTE: the CUSTOM config file (~/.pynbodyrc), if used, STILL must be present at runtime!
addedDataArgs='--add-data='${src}':'${dest}" "

# (*) ALTERNATIVELY:
# Store data in folder 'addfiles' and set e.g. src='addfiles/default_config.ini'

# add yt 'missing' data
# add here anyother missing (not found) modules in case the build fails
# see rave_standalone_error.png for an example of the error message in such a case. The look in baseDir for the missing module; if it's there, simply added to the following list (e.g. units in the case of this exmaple):
datadirs=( 'geometry' 'frontends' 'utilities' 'fields' 'units' )
baseDir='/Users/tepper/Library/Python/3.9/lib/python/site-packages/yt/'
dataFlag='--add-data='
for i in "${datadirs[@]}"
do
	arg=${baseDir}${i}':yt/'${i}
	addedDataArgs=${addedDataArgs}${dataFlag}${arg}" "
done

# NEW: As of 12 MAR 2024 (likely earlier) YT has delegated the use of colormaps to an independent module, cmyt.
src='/Users/tepper/Library/Python/3.9/lib/python/site-packages/cmyt'
dest='cmyt'
addedDataArgs=${addedDataArgs}'--add-data='${src}':'${dest}" "


# NEW: As of version 4.3 (likely earlier) YT makes use of the module ewah_bool_utils
src='/Users/tepper/Library/Python/3.9/lib/python/site-packages/ewah_bool_utils'
dest='ewah_bool_utils'
addedDataArgs=${addedDataArgs}'--add-data='${src}':'${dest}" "

# invoke PyInstaller's makespec
# Using --onedir instead of --onefile significantly speeds up launching the app
# command='pyi-makespec '${addedDataArgs}' --onefile --windowed --icon icons/rave.icns --name RAVE '${name}'.py'
command='pyi-makespec '${addedDataArgs}' --onedir --windowed --icon icons/rave.icns --name RAVE '${name}'.py'
echo -e '\nIssuing command:\n'${command}
echo
${command}

echo -e '\n[Ignore the previous two lines; it will be taken care of automatically]'

# This can be done by hand but it is safer this way:
echo -e '\nAdding higher recursion limit to spec file'
# must escape * with \:
insertLine="import sys ; sys.setrecursionlimit(sys.getrecursionlimit() \* 5)\n"
awkArg="awk 'NR==3{print \""${insertLine}"\"}1' "${name}'.spec'
# must invoke this way to honour the quotes and double-quotes
echo ${awkArg}  | sh > tmp.spec
mv tmp.spec ${name}'.spec'
echo -e '\nDone.\n'

# invoke PyInstaller:
command='pyinstaller '${name}'.spec'
echo -e '\nIssuing command:\n'${command}
${command}
echo -e '\nDone.\n'

echo -e '\nExecutables created in dist:'
ls dist
echo -e '\nDone.\n'

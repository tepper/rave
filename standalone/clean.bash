#!/bin/bash

command='rm -rf build dist *.spec __pycache__'

echo -e '\nIssuing command:\n'${command}
${command} | sh

echo -e '\nDone\n'

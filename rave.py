"""
========================================================================
AUTHOR:
	Dr. Thor Tepper-Garcia

CREATED:
	14/04/2020
	-> In continuous development!

ABOUT:
	Ramses Analysis and Visualisation Environment (RAVE) is a class to provide a *customised* Pynbody approach to the analysis of N-body / hydrodynamical simulations. Makes use of the rave_core.py and yt_utils.py modules.
========================================================================
"""
# Module imports

# NATIVE MODULES
import os
import traceback
from pathlib import Path

# Required to deter the standalone to open multiple GUI windows
# See call to main() at the bottom of this file
import multiprocessing

# GUI relevant modules
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox

# Graph modules
import matplotlib
from matplotlib.figure import Figure
import matplotlib.colors as mcolors # to include a colorbar
import matplotlib.cm 				# colormaps
from matplotlib.ticker import LogFormatterExponent # to format tics
# IMPORTANT: If matplotlib was installed with MacPorts, make sure
# that it has the +tkinter variant, otherwise the following import
# will fail (!):
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

from scipy.ndimage import gaussian_filter # to smooth data

import yaml

# EXTERNAL MODULES
import rave_core
import yt_utils

# ---------- End of module import --------

# matplotlib backend
matplotlib.use('TkAgg')
# matplotlib.use('TkAgg','fast') # supposed to speed up plotting

class Pynbody_Ramses_GUI():

	def __init__(self,master):


		# Default variable settings / values
		self.file_list = []
		self.snaplist_dict = {}
		self.comp_list = []

		self._homepath = str(Path.home())

		# ADJUST the following for standalone deployment:
		self._initdir = self._homepath + \
			'/mycodes/PYTHON3/ramses_tools/GUI_tools/test_data/'
		# failsafe: # rever to home dir if dir non-existent
		if not os.path.isdir(self._initdir):
			self._initdir = self._homepath + '/'

		self._initdir_alt = self._homepath + \
			'/codes/agama/agama/myics/'
		if not os.path.isdir(self._initdir_alt):
			self._initdir_alt = self._homepath + '/'

		self.debug = True # change to False/True to de/activate debug options

		self.gas = False # flag for gas components
		self._boxed_legend = False	# controls whether plot legend is framed
		self.canvas_is_free = True	# controls risk of overplotting
		self.MaxCompNumber_default = 11
		self.compids_var_default = 1
		self.skipevery_var_default = 1
		self.com_var_default = 1
		self.cov_var_default = 1
		self.alignangmom_var_default = 1
		self.xmin_var_default = -20.
		self.xmax_var_default = 20.
		self.ymin_var_default = -20.
		self.ymax_var_default = 20.
		self.ymin_var_default_str = '<auto>'
		self.ymax_var_default_str = '<auto>'
		self.cbmin_var_default = '1.e4'
		self.cbmax_var_default = '1.e10'
		self.resolution_var_map_default = 256
		self.resolution_var_prof_default = 100
		self.default_fontsize = matplotlib.rcParams.get('font.size')

		# field size settings, etc.
		self.progressbar_length = 320
		self.field_width = 11
		self.dropdown_field_width = 11

		# save xy plot data to ascii
		self.xytable = None
		# save map / contour data to binary
		self.xymap = None

		# plot types
		self.options_list = ['<Select a plot type>',
			'generic_weighted_map',
			'generic_contour_map',
			'generic_radial_profile',
			'generic_3D_scatterplot'
		]

		# plot styles and colors
		self.lstyle_list = ['<Style>','None',
			'solid','dashed','dashdot','dotted'
		]
		# see:
		# https://matplotlib.org/api/colors_api.html#module-matplotlib.colors
		self.lcolor_list = ['<Color>',
			'blue','green','red','cyan','magenta','yellow','black','white',
			'tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
			'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan'
		]
		# for a meaning of the marker symbols and a full list see:
		# https://matplotlib.org/api/markers_api.html#module-matplotlib.markers
		self.marker_list = ['<Marker>','None',
			'.',',','o','v','^','<','>',
			'1','2','3','4','s','p',
			'*','h','H','+','x','D','d','|','_'
		]
		# color map schemes
		# sequential (row 1,2)
		# perceptually uniform sequential (row 3)
		# diverging (row 4)
		# qualitative (row 5)
		# miscellaneous (row 6)
		# complete list:
		# dir(matplotlib.cm)
		# or:
		# https://matplotlib.org/gallery/color/->
		#->colormap_reference.html?highlight=colormaps
		# NOTE: Additional matplotlib valid colormap names not contained in this
		# list may also be typed in directly into the corresponding GUI field
		self.colormaps_list = [ '<Select or type>',
			'Greys',
			'OrRd', 'PuRd', 'RdPu', 'BuPu', 'GnBu', 'PuBu', 'PuBuGn', 'BuGn',
			'viridis', 'plasma', 'inferno', 'magma', 'cividis',
			'PiYG','RdYlGn','bwr',
			'Paired', 'tab20c',
			'jet','gnuplot'
			]

		# Geometric filters
		self._r1_filter_var = tk.DoubleVar()
		self._r2_filter_var = tk.DoubleVar()
		self._orig_filter_var = tk.StringVar()
		self._radius_filter_var = tk.DoubleVar()
		self._height_filter_var = tk.DoubleVar()
		self._angle_min_filter_var = tk.DoubleVar()
		self._angle_max_filter_var = tk.DoubleVar()
		self._side_filter_var = tk.DoubleVar()

		# Geometric transformations
		self._alpha_transform_var = tk.DoubleVar() # rot. angle (deg) around x
		self._beta_transform_var = tk.DoubleVar() # rot. angle (deg) around y
		self._gamma_transform_var = tk.DoubleVar() # rot. angle(deg)  around z

		# quantity-based filter
		self._quantity_filter_var = tk.StringVar()
		self._quantity_max_filter_var = tk.DoubleVar()
		self._quantity_min_filter_var = tk.DoubleVar()

		# contour variables
		self.contour_sigma_var = tk.DoubleVar()
		self.contour_levels_var = tk.StringVar()
		self.contour_color_var = tk.StringVar()

		# special variables and functions
		self.omega_p_var = tk.DoubleVar()

		# error catching
		# report_callback_exception is a method of tkinter.Tk instance
		self.err_msg_length = 2 # remove anything beyond these many lines
		self.err_msg_fontsize = 10
		master.report_callback_exception = \
			lambda etype,value,tb:\
			self._report_callback_except(etype, value, tb, win=master)

		# top-level window settings
		_w = 950	# used to position GUI window
		_h = 620
		# get screen width and height
		_ws = master.winfo_screenwidth() # width of the screen
		_hs = master.winfo_screenheight() # height of the screen
		# calculate x and y coordinates for the Tk root window
		_winx = int((_ws/2) - (_w/1.6))
		_winy = int((_hs/2) - (_h/1.45))

		# place windown on the screen without specifying its size
		# master.geometry('{}x{}+{}+{}'.format(_ws,_hs,_winx, _winy))
		master.geometry('+{}+{}'.format(_winx, _winy))
		master.resizable(width=False, height=False)
		self._gui_title='Ramses Analysis and Visualisation Environment (RAVE)'
		master.title(self._gui_title)

		# create (dropdown) menu bar...
		master.option_add('*tearOff',False) # prevent menus from being torn off
		self.menubar = tk.Menu(master)
		master.config(menu=self.menubar)
		# ...and its items
		self.menu_snapshot = tk.Menu(self.menubar)
		self.menu_help = tk.Menu(self.menubar)
		self.menubar.add_cascade(menu=self.menu_snapshot,label='File')
		self.menubar.add_cascade(menu=self.menu_help,label='Help')
		# add command entries to items
		self.menu_snapshot.add_command(label='Add snapshot (dir)...',
			command=lambda:self._open_snapshot(master))
		self.menu_snapshot.add_command(label='Add snapshot (file)...',
			command=lambda:self._open_icfile(master))
		self.menu_snapshot.add_command(label='Load map (npy)...',
			command=lambda:messagebox.showwarning(message='Not yet implemented'))
		self.menu_snapshot.add_command(label='Load table (ascii)...',
			command=lambda:messagebox.showwarning(message='Not yet implemented'))
		self.menu_snapshot.add_separator()
		self.menu_snapshot.add_command(label='Quit',
			command=lambda:self._quit_gui(master))
		self.menu_help.add_command(label='About',
			command=lambda:self._display_about(master))

		# main frames
		self.controls_frame = ttk.LabelFrame(master,text='Control Panel')
		# self.controls_frame.config(width=150,height=500)
		self.controls_frame.grid(row=0,column=0,rowspan=2,columnspan=4,padx=10,pady=4)

		self.fileinfo_frame = ttk.LabelFrame(master,text='Added files:')
		self.fileinfo_frame.grid(row=0,column=5,padx=5,pady=0)

		self.display_frame = ttk.LabelFrame(master,text='Display')
		# self.display_frame.config(width=3000,height=500)
		self.display_frame.grid(row=1,column=5,padx=10,pady=4)

		# CONTROL PANEL

		# IN DEVELOPMENT
		_row_num_control=0
		self.preload_button = ttk.Button(self.controls_frame,text='Load',
			command=lambda:self._preload_snapshot(master))
		self.preload_button.grid(row=_row_num_control,column=0,
			padx=5,pady=4,sticky='news')

		self.reset_button = ttk.Button(self.controls_frame,text='Unload',
			command=lambda:self._unload_snapshot(master))
		self.reset_button.grid(row=_row_num_control,column=1,
			padx=5,pady=4,sticky='news')

		self.quit_button = ttk.Button(self.controls_frame,text='Quit',
			command=lambda:self._quit_gui(master))
		self.quit_button.grid(row=_row_num_control,column=3,
			padx=5,pady=4,sticky='news')

		# ACTIVATE for development ONLY
		# Adjust the command as necessary
		self.test_button = ttk.Button(self.controls_frame,text='Test',
			command=lambda:self._dummy_function(master))
		self.test_button.grid(row=_row_num_control,column=2,
			padx=5,pady=4,sticky='news')

		# progressbar
		_row_num_control+=1
		self.progressbar_load = ttk.Progressbar(self.controls_frame,
			orient='horizontal',length=self.progressbar_length,
			mode='determinate')
		self.progressbar_load.grid(row=_row_num_control,column=0,columnspan=4,
			padx=5,pady=4)

		# SIMULATION PREVIEW
		_row_num_control+=1
		self.overview_frame = ttk.LabelFrame(self.controls_frame, text='Overview')
		self.overview_frame.grid(row=_row_num_control,column=0,columnspan=4,padx=10,pady=2)
		_row_num_overview=0
		# Number of particles
		self.simopreview_npart_var = tk.DoubleVar()
		self.simopreview_npart_var.set(0)
		self.simopreview_npart_label = ttk.Label(self.overview_frame,
			text='Particles:')
		self.simopreview_npart_field = ttk.Entry(self.overview_frame,
			textvariable=self.simopreview_npart_var)
		self.simopreview_npart_field.config(width=9)
		self.simopreview_npart_label.grid(row=_row_num_overview,column=0,
			padx=5,pady=4,sticky='e')
		self.simopreview_npart_field.grid(row=_row_num_overview,column=1,
			padx=5,pady=4,sticky='w')
		self.simopreview_npart_field.config(state='readonly')
		# Number of gas cells
		self.simopreview_ncell_var = tk.DoubleVar()
		self.simopreview_ncell_var.set(0)
		self.simopreview_npart_label = ttk.Label(self.overview_frame,
			text='Cells:')
		self.simopreview_ncell_field = ttk.Entry(self.overview_frame,
			textvariable=self.simopreview_ncell_var)
		self.simopreview_ncell_field.config(width=9)
		self.simopreview_npart_label.grid(row=_row_num_overview,column=2,
			padx=5,pady=4,sticky='e')
		self.simopreview_ncell_field.grid(row=_row_num_overview,column=3,
			padx=5,pady=4,sticky='w')
		self.simopreview_ncell_field.config(state='readonly')
		# Refinement Levels
		_row_num_overview+=1
		self.simopreview_levmin_var = tk.IntVar()
		self.simopreview_levmin_var.set(0)
		self.simopreview_levmin_label = ttk.Label(self.overview_frame,
			text='Min.Ref.Level:')
		self.simopreview_levmin_field = ttk.Entry(self.overview_frame,
			textvariable=self.simopreview_levmin_var)
		self.simopreview_levmin_field.config(width=9)
		self.simopreview_levmin_label.grid(row=_row_num_overview,column=0,
			padx=5,pady=4,sticky='e')
		self.simopreview_levmin_field.grid(row=_row_num_overview,column=1,
			padx=5,pady=4,sticky='w')
		self.simopreview_levmin_field.config(state='readonly')
		self.simopreview_levmax_var = tk.IntVar()
		self.simopreview_levmax_var.set(0)
		self.simopreview_levmax_label = ttk.Label(self.overview_frame,
			text='Max.Ref.Level:')
		self.simopreview_levmax_field = ttk.Entry(self.overview_frame,
			textvariable=self.simopreview_levmax_var)
		self.simopreview_levmax_field.config(width=9)
		self.simopreview_levmax_label.grid(row=_row_num_overview,column=2,
			padx=5,pady=4,sticky='e')
		self.simopreview_levmax_field.grid(row=_row_num_overview,column=3,
			padx=5,pady=4,sticky='w')
		self.simopreview_levmax_field.config(state='readonly')
		# Box size
		_row_num_overview+=1
		self.simopreview_box_var = tk.DoubleVar()
		self.simopreview_box_var.set(0)
		self.simopreview_box_label = ttk.Label(self.overview_frame,
			text='Box size [kpc]:')
		self.simopreview_box_field = ttk.Entry(self.overview_frame,
			textvariable=self.simopreview_box_var)
		self.simopreview_box_field.config(width=9)
		self.simopreview_box_label.grid(row=_row_num_overview,column=0,
			padx=5,pady=4,sticky='e')
		self.simopreview_box_field.grid(row=_row_num_overview,column=1,
			padx=5,pady=4,sticky='w')
		self.simopreview_box_field.config(state='readonly')
		# Time
		self.simopreview_time_var = tk.DoubleVar()
		self.simopreview_time_var.set(0)
		self.simopreview_time_label = ttk.Label(self.overview_frame,
			text='Time [Myr]:')
		self.simopreview_time_field = ttk.Entry(self.overview_frame,
			textvariable=self.simopreview_time_var)
		self.simopreview_time_field.config(width=9)
		self.simopreview_time_label.grid(row=_row_num_overview,column=2,
			padx=5,pady=4,sticky='e')
		self.simopreview_time_field.grid(row=_row_num_overview,column=3,
			padx=5,pady=4,sticky='w')
		self.simopreview_time_field.config(state='readonly')
		# Families
		_row_num_overview+=1
		self.simpreview_fam_var = tk.DoubleVar()
		self.simpreview_fam_var.set('')
		self.simpreview_fam_label = ttk.Label(self.overview_frame,
			text='Families [tags]:')
		self.simpreview_fam_field = tk.Text(self.overview_frame, wrap='word')
		self.simpreview_fam_field.config(width=35,height=2)
		self.simpreview_fam_label.grid(row=_row_num_overview, column=0,
			padx=10,pady=4,sticky='e')
		self.simpreview_fam_field.grid(row=_row_num_overview, column=1, columnspan=3, padx=5,pady=4,sticky='w')
		self.simpreview_fam_field.config(state='normal')

		# COMPONENT SELECTION
		_row_num_control+=1
		self.selection_frame = ttk.LabelFrame(self.controls_frame,
			text='Family (component) selection')#,width=400, height=200)
		self.selection_frame.grid(row=_row_num_control,column=0,columnspan=4,padx=10,pady=4)
		# self.selection_frame.grid_propagate(False)

		_row_num_selection=0
		self.selectype_var = tk.IntVar()
		self.selectype_var.set(0) # unchecked
		self.selectype_button = tk.Checkbutton(self.selection_frame,
			variable=self.selectype_var, text='By CompID')
		self.selectype_button.grid(row=_row_num_selection,column=0,
			padx=0,pady=4,sticky='w')

		# The following two shall become 'legacy' settings, and replaced by selections via families and tags
		# MaxCompNumber
		_row_num_selection+=1
		self.maxcompnum_var = tk.IntVar()
		self.maxcompnum_var.set(self.MaxCompNumber_default)
		self.maxcompnum_label = ttk.Label(self.selection_frame,
			text='MaxCompNum:')
		self.maxcompnum_field = ttk.Entry(self.selection_frame,
			textvariable=self.maxcompnum_var)
		self.maxcompnum_field.config(width=3, state='disabled')
		self.maxcompnum_label.grid(row=_row_num_selection,column=0,
			padx=2,pady=4,sticky='news')
		self.maxcompnum_field.grid(row=_row_num_selection,column=1,
			padx=2,pady=4,sticky='news')
		# Comp IDs
		self.compids_var = tk.StringVar()
		self.compids_var.set(self.compids_var_default)
		self.compids_label = ttk.Label(self.selection_frame,
			text='Comp IDs (csv):')
		self.compids_field = ttk.Entry(self.selection_frame,
			textvariable=self.compids_var)
		self.compids_field.config(width=6, state='disabled')
		self.compids_label.grid(row=_row_num_selection,column=2,
			padx=2,pady=1,sticky='news')
		self.compids_field.grid(row=_row_num_selection,column=3,padx=2,pady=1,sticky='w')
		# Remove all four widgets right away, only to be attached if selectype_button is checked:
		self.remove_widget(self.maxcompnum_label)
		self.remove_widget(self.maxcompnum_field)
		self.remove_widget(self.compids_label)
		self.remove_widget(self.compids_field)

		# NEW: selection based on families and tags
		_row_num_selection+=1
		self.famtags_dict_str = {} # family as str (e.g. 'dm') and tags
		self.famtags_dict_map = {} # map family str <-> obj (e.g. <Family dm>)
		self.famtags_var = tk.StringVar()
		self.famtags_var.set('')
		self.famtags_label = ttk.Label(self.selection_frame,
			text='Fam:[tags] (csv)')
		self.famtags_field = ttk.Entry(self.selection_frame,
			textvariable=self.famtags_var)
		self.famtags_field.config(width=32)
		self.famtags_label.grid(row=_row_num_selection, column=0,
			padx=5, pady=4,sticky='news')
		self.famtags_field.grid(row=_row_num_selection, column=1, columnspan=3,
			padx=5, pady=4, sticky='news')


		# Skip
		_row_num_selection+=1
		self.skipevery_var = tk.IntVar()
		self.skipevery_var.set(self.skipevery_var_default)
		self.skipevery_label = ttk.Label(self.selection_frame,
			text='Select every Nth particle; N=')
		self.skipevery_field = ttk.Entry(self.selection_frame,
			textvariable=self.skipevery_var)
		self.skipevery_field.config(width=4)
		self.skipevery_label.grid(row=_row_num_selection,column=0, columnspan=2,
			padx=5,pady=4,sticky='news')
		self.skipevery_field.grid(row=_row_num_selection,column=2,
			padx=5,pady=4,sticky='news')

		# buttons
		_row_num_selection+=1
		self.com_var = tk.IntVar()
		self.com_var.set(self.com_var_default)
		self.com_button = tk.Checkbutton(self.selection_frame,
			variable=self.com_var, text='CoM')
		self.com_button.grid(row=_row_num_selection,column=0,
			padx=0,pady=4,sticky='news')

		self.cov_var = tk.IntVar()
		self.cov_var.set(self.com_var_default)
		self.cov_button = tk.Checkbutton(self.selection_frame,
			variable=self.cov_var,text='CoV')
		self.cov_button.grid(row=_row_num_selection,column=1,
			padx=0,pady=4,sticky='news')

		self.alignangmom_var = tk.IntVar()
		self.alignangmom_var.set(self.alignangmom_var_default)
		self.alignangmom_button = tk.Checkbutton(self.selection_frame,
			variable=self.alignangmom_var, text='L || z')
		self.alignangmom_button.grid(row=_row_num_selection,column=2,
			padx=0,pady=4,sticky='news')

		# _row_num_control+=1
		self.load_button = ttk.Button(self.selection_frame, text='Select',
			command=lambda:self._load_snapshot(master))
		self.load_button.grid(row=_row_num_selection,column=3,
			padx=5,pady=4,sticky='news')

		# simulation overview
		self.simoverview_npart_var = tk.DoubleVar()
		self.simoverview_npart_var.set(0)
		self.simoverview_time_var = tk.DoubleVar()
		self.simoverview_time_var.set(0)

		# create 'plot' sub-frame
		# entry fields
		_row_num_control+=4
		self.options_var = tk.StringVar()
		self.options_funcs = {\
			self.options_list[1]: self._plot_generic_weighted_map,
			self.options_list[2]: self._plot_generic_contour_map,
			self.options_list[3]: self._plot_generic_radial_profile,
			self.options_list[4]: self._plot_generic_3D_scatterplot,
			}
		self.options_var.set(self.options_list[0])
		self.options_field = ttk.Combobox(self.controls_frame,
			values=self.options_list,textvariable=self.options_var,
			width=21)
		self.options_field.grid(row=_row_num_control,column=0,columnspan=2,
			padx=5,pady=4)

		# filters
		# _row_num_control+=1
		self.filter_var = tk.StringVar()
		self.filter_list = ['<Selection Filter>',
			'All',
			'SolarNeighborhood',
			'Sphere',
			'Cuboid',
			'Disc',
			'CircularSector',
			'Arc',
			'QuantityFilter'
			]
		self.filter_var.set(self.filter_list[0])
		self.filter_field = ttk.Combobox(self.controls_frame,
			values=self.filter_list,textvariable=self.filter_var,
			width=21)
		self.filter_field.grid(row=_row_num_control,column=2,columnspan=2,
			padx=5,pady=4)

		# transformations
		_row_num_control+=1
		self.transform_label = ttk.Label(self.controls_frame,
			text='Geometric transformation:')
		self.transform_label.grid(row=_row_num_control,column=0,columnspan=2,
			padx=5,pady=4,sticky='e')
		self.transform_var = tk.StringVar()
		self.transform_list = [
			'None',
			'Rotate',
			# 'Shift'
			]
		self.transform_var.set(self.transform_list[0])
		self.transform_field = ttk.Combobox(self.controls_frame,
			values=self.transform_list,textvariable=self.transform_var,
			width=21)
		self.transform_field.grid(row=_row_num_control,column=2,columnspan=2,
			padx=5,pady=4)

		self.xvalue_list = [ '<x-value>',
			'x',
			'y',
			'z',
			'rxy',
			'vx',
			'vy',
			'vz',
			'vrxy','vphi',
			'jz',
			'J_R','J_z','J_phi','energy','Omega_z','e_Jacobi','psi','omega',
			]
		self.yvalue_list = ['<y-value>']
		self.yvalue_list.extend(self.xvalue_list[1:])
		self.yvalue_list.extend([
			'surfdensity',
			'voldensity',
			'vel_R_disp',
			'vel_z_disp',
			'v_circ',
			'omega',
			'kappa',
			'Toomre_Q_star',
			'Toomre_Q_gas',
			'mass',
			'mass_enc',
			'pot',
			'temp',
			'p'
		])
		self.wvalue_list = ['<field>',
			'uniform',
			'mass',
			'temp',
			'p',
			'level'
			]
		self.wvalue_list.extend(self.xvalue_list[1:])
		_row_num_control+=1
		self.xvalue_var = tk.StringVar()
		self.xvalue_var.set(self.xvalue_list[0])
		self.xvalue_field = ttk.Combobox(self.controls_frame,
			values=self.xvalue_list,textvariable=self.xvalue_var,
			width=self.dropdown_field_width)
		self.xvalue_field.grid(row=_row_num_control,column=0,padx=5,pady=4)
		self.yvalue_var = tk.StringVar()
		self.yvalue_var.set(self.yvalue_list[0])
		self.yvalue_field = ttk.Combobox(self.controls_frame,
			values=self.yvalue_list,textvariable=self.yvalue_var,
			width=self.dropdown_field_width)
		self.yvalue_field.grid(row=_row_num_control,column=1,padx=5,pady=4)
		self.wvalue_var = tk.StringVar()
		self.wvalue_var.set(self.wvalue_list[0])
		self.wvalue_field = ttk.Combobox(self.controls_frame,
			values=self.wvalue_list,textvariable=self.wvalue_var,
			width=self.dropdown_field_width)
		self.wvalue_field.grid(row=_row_num_control,column=2,padx=5,pady=4)
		self.svalue_list = ['<statistic>',
			'mean','std','median',
			'count',
			'sum','min','max'
			]
		# _row_num_control+=1
		self.svalue_var = tk.StringVar()
		self.svalue_var.set(self.svalue_list[0])
		self.svalue_field = ttk.Combobox(self.controls_frame,
			values=self.svalue_list,textvariable=self.svalue_var,
			width=self.dropdown_field_width)
		self.svalue_field.grid(row=_row_num_control,column=3,padx=5,pady=4)

		_row_num_control+=1
		self.xmin_label = ttk.Label(self.controls_frame,
			text='x-axis (min)')
		self.xmin_label.grid(row=_row_num_control,column=0,padx=5,pady=4)
		self.xmax_label = ttk.Label(self.controls_frame,
			text='x-axis (max)')
		self.xmax_label.grid(row=_row_num_control,column=1,padx=5,pady=4)
		self.xmax_label = ttk.Label(self.controls_frame,
			text='Scale')
		self.xmax_label.grid(row=_row_num_control,column=2,padx=5,pady=4)

		_row_num_control+=1
		self.xmin_var = tk.DoubleVar()
		self.xmin_var.set('{}'.format(self.xmin_var_default))
		self.xmin_field = ttk.Entry(self.controls_frame,
			textvariable=self.xmin_var,width=self.field_width)
		self.xmin_field.grid(row=_row_num_control,column=0,padx=5,pady=4)
		self.xmax_var = tk.DoubleVar()
		self.xmax_var.set('{}'.format(self.xmax_var_default))
		self.xmax_field = ttk.Entry(self.controls_frame,
			textvariable=self.xmax_var,width=self.field_width)
		self.xmax_field.grid(row=_row_num_control,column=1,padx=5,pady=4)
		self.xscale_var = tk.StringVar()
		self.xscale_var.set('linear')
		self.xscale_field = ttk.Combobox(self.controls_frame,
			values=['linear','log',
				'squareroot','squared','abs'],
			textvariable=self.xscale_var,
			width=self.dropdown_field_width)
		self.xscale_field.grid(row=_row_num_control,column=2,padx=5,pady=4)

		_row_num_control+=1
		self.ymin_label = ttk.Label(self.controls_frame,
			text='y-axis (min)')
		self.ymin_label.grid(row=_row_num_control,column=0,padx=5,pady=4)
		self.ymax_label = ttk.Label(self.controls_frame,
			text='y-axis (max)')
		self.ymax_label.grid(row=_row_num_control,column=1,padx=5,pady=4)
		self.ymax_label = ttk.Label(self.controls_frame,
			text='Scale')
		self.ymax_label.grid(row=_row_num_control,column=2,padx=5,pady=4)

		self.aspect_label = ttk.Label(self.controls_frame,
			text='Axes units ratio')
		self.aspect_label.grid(row=_row_num_control,column=3,columnspan=2,
			padx=5,pady=4)

		_row_num_control+=1
		self.ymin_var = tk.StringVar()
		self.ymin_var.set('{}'.format(self.ymin_var_default))
		self.ymin_field = ttk.Entry(self.controls_frame,
			textvariable=self.ymin_var,width=self.field_width)
		self.ymin_field.grid(row=_row_num_control,column=0,padx=5,pady=4)
		self.ymax_var = tk.StringVar()
		self.ymax_var.set('{}'.format(self.ymax_var_default))
		self.ymax_field = ttk.Entry(self.controls_frame,
			textvariable=self.ymax_var,width=self.field_width)
		self.ymax_field.grid(row=_row_num_control,column=1,padx=5,pady=4)
		self.yscale_var = tk.StringVar()
		self.yscale_var.set('linear')
		self.yscale_field = ttk.Combobox(self.controls_frame,
			values=['linear','log',
				'squareroot','squared','abs'],
			textvariable=self.yscale_var,
			width=self.dropdown_field_width)
		self.yscale_field.grid(row=_row_num_control,column=2,padx=5,pady=4)

		self.aspect_values = ['equal','auto','enter value (y/x)']
		self.aspect_var = tk.StringVar()
		self.aspect_var.set(self.aspect_values[1]) # = auto
		self.aspect_field = ttk.Combobox(self.controls_frame,
			values=self.aspect_values,
		textvariable=self.aspect_var)
		self.aspect_field.config(width=self.dropdown_field_width)
		self.aspect_field.grid(row=_row_num_control,column=3,padx=5,pady=4)

		_row_num_control+=1
		self.cbmin_label = ttk.Label(self.controls_frame,
			text='Map (min)')
		self.cbmin_label.grid(row=_row_num_control,column=0,padx=5,pady=4)
		self.cbmax_label = ttk.Label(self.controls_frame,
			text='Map (max)')
		self.cbmax_label.grid(row=_row_num_control,column=1,padx=5,pady=4)
		self.cbmax_label = ttk.Label(self.controls_frame,
			text='Scale')
		self.cbmax_label.grid(row=_row_num_control,column=2,padx=5,pady=4)
		self.resolution_label = ttk.Label(self.controls_frame,
			text='Resolution [px]')
		self.resolution_label.grid(row=_row_num_control,column=3,columnspan=2,
			padx=5,pady=4,sticky='e')

		_row_num_control+=1
		self.cbmin_var = tk.DoubleVar()
		self.cbmin_var.set('{}'.format(self.cbmin_var_default))
		self.cbmin_field = ttk.Entry(self.controls_frame,
			textvariable=self.cbmin_var,width=self.field_width)
		self.cbmin_field.grid(row=_row_num_control,column=0,padx=5,pady=4)
		self.cbmax_var = tk.DoubleVar()
		self.cbmax_var.set('{}'.format(self.cbmax_var_default))
		self.cbmax_field = ttk.Entry(self.controls_frame,
			textvariable=self.cbmax_var,width=self.field_width)
		self.cbmax_field.grid(row=_row_num_control,column=1,padx=5,pady=4)
		self.cbscale_var = tk.StringVar()
		self.cbscale_var.set('log')
		self.cbscale_field = ttk.Combobox(self.controls_frame,
			values=['log', 'linear', 'abs_log', 'abs'],textvariable=self.cbscale_var,
			width=self.dropdown_field_width)
		self.cbscale_field.grid(row=_row_num_control,column=2,padx=5,pady=4)
		self.resolution_var = tk.IntVar()
		self.resolution_var.set(self.resolution_var_map_default)
		self.resolution_field = ttk.Entry(self.controls_frame,
			textvariable=self.resolution_var)
		self.resolution_field.config(width=self.field_width)
		self.resolution_field.grid(row=_row_num_control,column=3,padx=5,pady=4)

		_row_num_control+=1
		self.interpol_var = tk.StringVar()
		# interpolation methods for 2D maps
		self.interpol_list = [ None,
			'none', 'nearest', 'bilinear', 'bicubic', 'spline16',
           'spline36', 'hanning', 'hamming', 'hermite', 'kaiser', 'quadric',
           'catrom', 'gaussian', 'bessel', 'mitchell', 'sinc', 'lanczos'
        ]
		self.interpol_var.set('nearest')
		self.interpol_label = ttk.Label(self.controls_frame,
			text='Interpolation:')
		self.interpol_field = ttk.Combobox(self.controls_frame,
			values=self.interpol_list,textvariable=self.interpol_var,
			width=18)
		self.interpol_label.grid(row=_row_num_control,column=0,
			padx=5,pady=4,sticky='e')
		self.interpol_field.grid(row=_row_num_control,column=1,columnspan=3,
			padx=5,pady=4,sticky='w')

		_row_num_control+=1
		self.colormaps_var = tk.StringVar()
		self.colormaps_var.set('Greys')
		self.colormaps_label = ttk.Label(self.controls_frame,
			text='Color map:')
		self.colormaps_field = ttk.Combobox(self.controls_frame,
			values=self.colormaps_list,textvariable=self.colormaps_var,
			width=18)
		self.colormaps_label.grid(row=_row_num_control,column=0,
			padx=5,pady=4,sticky='e')
		self.colormaps_field.grid(row=_row_num_control,column=1,columnspan=3,
			padx=5,pady=4,sticky='w')

		_row_num_control+=1
		self.lineprops_label = ttk.Label(self.controls_frame,
			text='Line properties:')
		self.lineprops_label.grid(row=_row_num_control,column=0,
			padx=5,pady=4,sticky='e')

		# _row_num_control+=1
		self.lstyle_var = tk.StringVar()
		self.lstyle_var.set(self.lstyle_list[2])
		self.lstyle_field = ttk.Combobox(self.controls_frame,
			values=self.lstyle_list,
			textvariable=self.lstyle_var,width=self.dropdown_field_width)
		self.lstyle_field.grid(row=_row_num_control,column=1,padx=5,pady=4)
		self.lcolor_var = tk.StringVar()
		self.lcolor_var.set(self.lcolor_list[1])
		self.lcolor_field = ttk.Combobox(self.controls_frame,
			values=self.lcolor_list,
			textvariable=self.lcolor_var,width=self.dropdown_field_width)
		self.lcolor_field.grid(row=_row_num_control,column=2,padx=5,pady=4)
		self.marker_var = tk.StringVar()
		self.marker_var.set(self.marker_list[1])
		self.marker_field = ttk.Combobox(self.controls_frame,
			values=self.marker_list,textvariable=self.marker_var,
			width=self.dropdown_field_width)
		self.marker_field.grid(row=_row_num_control,column=3,padx=5,pady=4)

		_row_num_control+=1
		self.colorbar_var = tk.IntVar()
		self.colorbar_var.set(1)
		self.colorbar_button = tk.Checkbutton(self.controls_frame,
			text='Colorbar',
			variable=self.colorbar_var)
		self.colorbar_button.grid(row=_row_num_control,column=0,
			padx=5,pady=4)

		self.colorbar_orientation_var = tk.IntVar()
		self.colorbar_orientation_var.set(1)
		self.colorbar_orientation_button = tk.Checkbutton(self.controls_frame,
			text='CB Vertical',
			variable=self.colorbar_orientation_var)
		self.colorbar_orientation_button.grid(row=_row_num_control,column=1,
			padx=5,pady=4)

		self.custom_plotlabels_var = tk.IntVar()
		self.custom_plotlabels_var.set(0)
		self.custom_plotlabels_button = tk.Checkbutton(self.controls_frame,
			text='Custom plot labels and legends',
			variable=self.custom_plotlabels_var)
		self.custom_plotlabels_button.grid(row=_row_num_control,column=2,
			columnspan=2,padx=5,pady=4)

		# plot-relevant buttons
		_row_num_control+=1
		self.plot_button = ttk.Button(self.controls_frame,text='Plot',
			command=lambda:self._plot(master))
		self.plot_button.grid(row=_row_num_control,column=0,padx=5,pady=4)
		self.clear_button = ttk.Button(self.controls_frame,text='Clear',
			command=lambda:self._clear_plot(master))
		self.clear_button.grid(row=_row_num_control,column=1,padx=5,pady=4)
		self.save_plot_button = ttk.Button(self.controls_frame,text='Save plot',
			command=self._save_plot)
		self.save_plot_button.grid(row=_row_num_control,column=2,padx=5,pady=4)
		self.save_data_button = ttk.Button(self.controls_frame,text='Save data',
			command=self._save_data)
		self.save_data_button.grid(row=_row_num_control,column=3,padx=5,pady=4)

		# plot progressbar
		_row_num_control+=2
		self.progressbar_plot = ttk.Progressbar(self.controls_frame,
			orient='horizontal',length=self.progressbar_length,
			mode='determinate')
		self.progressbar_plot.grid(row=_row_num_control,
			column=0,columnspan=4,padx=5,pady=4)

		# create file info field
		self.filelist_field = tk.Text(self.fileinfo_frame)
		self.filelist_field.config(width=99,height=4,padx=5,pady=0)
		self.filelist_field.pack()

		# configure display
		self.figure = Figure(figsize=(7, 6))
		# self.figure = Figure() # do not to set size and dpi by hand
		# create graph widget (slave to display_frame)
		self.canvas = FigureCanvasTkAgg(self.figure, self.display_frame)
		self.canvas.get_tk_widget().pack()
		self._panel1 = self.figure.add_subplot(1,1,1)

		# EVENT-TRIGGERED BEHAVIOUR, e.g. by changing selected widgets' values
		# NOTE: not really sure why and how the following works, but it does...
		# I guess the class and its contents are contained within the loop
		# once it is instantiated
		# sanity settings
		self.selectype_var.trace_add('write',
			lambda a,b,c:self._activate_legacy_selection_fields())
		self.maxcompnum_var.trace_add('write',
			lambda a,b,c:self._set_load_entries_maxcompnum())
		self.com_var.trace_add('write',
			lambda a,b,c:self._set_load_entries_com())
		self.cov_var.trace_add('write',
			lambda a,b,c:self._set_load_entries_cov())
		self.alignangmom_var.trace_add('write',
			lambda a,b,c:self._set_load_entries_angmom())
		# filter options
		self.filter_var.trace_add('write',
			lambda a,b,c:self._filter_settings(master))
		#transform options
		self.transform_var.trace_add('write',
			lambda a,b,c:self._transform_settings(master))
		# special cases
		self.wvalue_var.trace_add('write',
			lambda a,b,c:self._special_cases(master))
		# disable all plot entries
		self._disable_plot_entries()
		# optional: Set plot entries according to plot option chosen
		# Intent is to make GUI 'fool-proof'
		self.options_var.trace_add('write',
			lambda a,b,c:self._set_plot_entries(master))
		self.yvalue_var.trace_add('write',
			lambda a,b,c:self._reset_yrange_fields())
		self.custom_plotlabels_var.trace_add('write',
			lambda a,b,c:self._set_custom_plotlabels(master))

		# set custome styles (must be done here after everything else)
		style = ttk.Style()
		style.map('TCombobox', fieldbackground=[('readonly','white')])
		style.map('TCombobox', selectbackground=[('readonly', 'white')])
		style.map('TCombobox', selectforeground=[('readonly', 'black')])


		# move this to an info field, perhaps above 'Open files:'
		messagebox.showinfo(title='Threading',
			message='Pynbody running on:\n {} threads / {} cpus'.
				format(rave_core.pynbody.config['number_of_threads'],
					rave_core.openmp.get_cpus()))

# End of __init__

	# functions and methods
	def _dummy_function(self, win=None):
		'''
		A dummy function to test button click events
		'''
		message_text = 'You clicked a button in development!'
		win.option_add('*Dialog.msg.font', 'Helvetica 11')
		messagebox.showinfo(title='ALERT',message=message_text)
		win.option_clear() # undo font setting


	def _parse_famtags(self, win=None):
		'''
		Parse the string content of the famtags_field into a proper dict
		'''
		famtag_string = self.famtags_var.get()
		# add {} to string (double {{}} to yield {})
		famtag_string = f'{{{famtag_string}}}'
		famtag_dict = yaml.safe_load(famtag_string) # parse str -> dict
		return famtag_dict


	def _report_callback_except(self, *args, win=None):
		win.option_add('*Dialog.msg.font',
			f'Arial {self.err_msg_fontsize}') # messagebox font config
		err = traceback.format_exception(*args) # err is a list
		if self.debug:
			print(err)	# visible on a terminal if ran from source
		if len(err) > self.err_msg_length:
			err = err[-self.err_msg_length:]
			err.insert(0,'[Abridged]')
		messagebox.showerror('Exception', err)
		win.option_clear() # call this to reset


	def _display_about(self,win=None):
		'''
		Display brief information about this GUI
		'''
		message_text=\
			'A GUI to rave_core.py, a class that provides a '+\
			'*customised* Pynbody approach to the analysis of N-body/hydrodynamical simulations' +\
			'It is intended to be used mainly with the '+\
			'output of *customised* simulations ran with the Ramses code, '+\
			'but may be used also for more general cases.'
		win.option_add('*Dialog.msg.font', 'Helvetica 11')
		messagebox.showinfo(title='About',message=message_text)
		win.option_clear() # undo font setting


	def _quit_gui(self,win):
		if messagebox.askyesno(message='Do you really want to quit?'):
			print('\nCleanly quitting RAVE...')
			win.quit()
			win.destroy()
			print('Done.\n')


	def _update_pb(self,win=None,pb=None,val=None):
		if win is None or pb is None or val is None:
			raise ValueError('Expected three arguments, got less.')
		pb['value'] = val
		win.update()	# may not be appropriate or even correct (performance)


	def _update_snapdict(self,filen=None):
		'''
		Update snapfile dictionary
		'''
		for filen in self.file_list:
			if filen not in self.snaplist_dict:
				self.snaplist_dict[filen] = None


	def _set_filelist(self):
		'''
		Return file list from fileinfo field and updates file list variable
		'''
		_text_content = self.filelist_field.get('1.0','end-1c')
		flist = _text_content.split('\n')
		self.file_list = \
			[i for i in flist if i != ''] # get rid of empty entries


	def _show_filelist(self):
		'''
		Print content of file list and associated var to stdout
		Intended for debugging only
		'''
		print('File list:\n{}'.format(self.file_list))


	def _open_snapshot(self,win=None):
		'''
		Adds a snapshot directory (typically Ramses) to the file list (but does not load any data)
		'''
		self._update_pb(win,self.progressbar_load,0)
		self._update_pb(win,self.progressbar_plot,0)
		# request file name from user
		_snapfile = filedialog.askdirectory(initialdir=self._initdir)
		print('_open_snapshot', _snapfile)
		# update filelist field, file list, and snapfile dict
		if _snapfile is not None and _snapfile != () and \
			_snapfile != '': # user clicks 'cancel'
				self.file_list.append(_snapfile) # _set_filelist doesn't work
				_len = len(self.file_list)
				self.filelist_field.insert(float(_len),_snapfile+'\n')
				if self.debug:
					self._show_filelist()
				# update snaplist dict (indirectly)
# 				self._update_snapdict()


	def _open_icfile(self,win=None):
		'''
		Adds a snapshot file (typically initial conditions in Gadget-2 format generated with e.g. AGAMA) to the file list (but does not load any data)
		'''
		self._update_pb(win,self.progressbar_load,0)
		self._update_pb(win,self.progressbar_plot,0)
		# request file name from user
		_icfile = filedialog.askopenfilename(initialdir=self._initdir_alt)
		print('_open_icfile', _icfile)
		# update filelist field, file list, and snapfile dict
		if _icfile is not None and _icfile != () and \
			_icfile != '': # user clicks 'cancel'
				self.file_list.append(_icfile) # _set_filelist doesn't work
				_len = len(self.file_list)
				self.filelist_field.insert(float(_len), _icfile+'\n')
				if self.debug:
					self._show_filelist()


	# IN DEVELOPMENT
	def _simulation_preview(self):
		'''
		Provide some useful information and properties about the simulation(s)
		'''
		fams = {}
		npart = 0
		ncell = 0
		# concatenate families and tags for each snapshot avoiding repetition
		for sf in self.snaplist_dict:
			snap = self.snaplist_dict[sf]
			ncell += len(snap.snap_object.g)
			npart += len(snap.snap_object) - len(snap.snap_object.g)
			fam = snap.snap_object.families()
			for f in fam:
				try: # initialise if not existent
					fams[f]
				except:
					fams[f] = []
				try:
					tags = list(set(snap.snap_object[f]['tag']))
				except:
					tags = [0]
				fams[f] += (tags)
				fams[f] = list(set(fams[f]))
		families = [f'{k}: {fams[k]}' for k in fams]
		families = ', '.join(families)
		for fam in fams:
			self.famtags_dict_map[f'{fam}'] = fam
		self.simpreview_fam_field.insert('1.0', families)
		self.simpreview_fam_field.config(state='disabled') # make readonly
		self.simopreview_npart_var.set('{:.3E}'.format(npart))
		self.simopreview_ncell_var.set('{:.3E}'.format(ncell))
		# these only make sense for a single snapshot; for now using the last one loaded:
		self.simopreview_time_var.set(f'{snap.timeMyr:.3E}')
		self.simopreview_box_var.set(f'{snap.snap_object._info["boxlen"]:.1f}')
		self.simopreview_levmin_var.set(f'{snap.snap_object._info["levelmin"]}')
		self.simopreview_levmax_var.set(f'{snap.snap_object._info["levelmax"]}')
		return


	def _simulation_overview(self):
		numpart = 0
		for sf in self.snaplist_dict:
			snap = self.snaplist_dict[sf]
			numpart += snap.num_part
		self.simoverview_npart_var.set('{:.3E}'.format(numpart))
		# this only makes sense for a single snapshot; thus:
		self.simoverview_time_var.set('{:.3E}'.format(snap.timeMyr))
		return


	def _unload_snapshot(self,win=None):
		'''
		Unload snapshot data from tmp memory and snapshot from list
		Two ways:
		1) remove by hand from fileinfo field; any order
		2) one by one in reversed order (i.e. starting with the last added)
		'''
		if win is None:
			raise ValueError('Need to provide a top-level window')

		#resets
		self.simoverview_npart_var.set(0)
		# in case user modified file list by-hand
		self._set_filelist()
		self.snaplist_dict.pop('',None) # get rid of empty items
		_slen = len(self.snaplist_dict)
		if _slen < 1:
			messagebox.showwarning(message="No snapshot to unload")
		else:
			self._set_filelist()
			_len = len(self.file_list)
			if _len >= _slen:
				_start = float(_len)
				_end = str(_len)+".end"
				self.filelist_field.delete(_start,_end)
			self._set_filelist()

			# remove snapdata from snapfile dict
			for snap in self.snaplist_dict:
				if snap not in self.file_list:
					self.snaplist_dict[snap] = None
			self.snaplist_dict = \
				{key:val for key,val in self.snaplist_dict.items() if \
				val is not None}

			# if no snapfile left, reset load progressbar
			if len(self.snaplist_dict) < 1:
				self._update_pb(win,self.progressbar_load,0)
			if not self.canvas_is_free:
				if messagebox.askyesno(message='Clear canvas first?',
					default='no'):
						self._clear_plot(win)
				else:
					self._update_pb(win,self.progressbar_plot,0)


	# IN DEVELOPMENT
	def _preload_snapshot(self,win=None):
		'''
		Lazy load a snapshot, retrieve some simulation properties, and display on GUI
		'''
		#reset
		self.simopreview_npart_var.set(0)
		self.simopreview_ncell_var.set(0)
		self.simopreview_levmin_var.set(0)
		self.simopreview_levmax_var.set(0)
		self.simopreview_box_var.set(0)
		self.simopreview_time_var.set(0)
		self.simpreview_fam_field.config(state='normal')
		self.simpreview_fam_field.delete('1.0','end')
		# call these again in case user modified file list by-hand
		self._set_filelist()
		self._update_snapdict()
		# sanity check(s)
		if len(self.snaplist_dict) < 1:
			messagebox.showwarning(title='Warning',
				message="No snapshot yet added (go to 'File')")
		else:
			self._update_pb(win,self.progressbar_plot,0)
			self._update_pb(win,self.progressbar_load,0)
			# progressbar steps (100 - 10 divided by number of files and steps)
			_pbstep = 90./len(self.snaplist_dict)/2

			self._update_pb(win,self.progressbar_load,5)

			for _snapfile in self.snaplist_dict:
				_snapobject = \
					rave_core.Snapshot_Processing(snap=_snapfile,
					maxcomp=self.maxcompnum_var.get(),
					complst=self.comp_list,skip=self.skipevery_var.get(),
					verbose=self.debug)
				self._update_pb(win,self.progressbar_load,
					self.progressbar_load['value']+_pbstep)
				self.snaplist_dict[_snapfile] = _snapobject

			self._update_pb(win,self.progressbar_load,100)
			# report simulation overview
			self._simulation_preview()
			return


	def _load_snapshot(self,win=None):
		'''
		Load snaphot data to memory for each snapshot file in list
		Progressbar is updated after eaach step below by hand
		to avoid using threads.
		'''
		#resets
		self.gas = False
		# self.options_var.set(self.options_list[0])
		# self.filter_var.set(self.filter_list[0])
		self.simoverview_npart_var.set(0)
		# in case user modified file list by-hand
		self._set_filelist()
		self._update_snapdict()
		# sanity check(s)
		if len(self.compids_var.get()) < 1:
			raise ValueError('Provide at least one component to plot')
		elif len(self.snaplist_dict) < 1:
			messagebox.showwarning(title='Warning',
				message="No snapshot yet added (got to 'File')")
		else:
			self._update_pb(win,self.progressbar_plot,0)
			self._update_pb(win,self.progressbar_load,0)
			# progressbar steps (100 - 10 divided by number of files and steps)
			_pbstep = 90./len(self.snaplist_dict)/5

			# cast input csv list into a python list; the use of int(float)
			# takes care of user incorrectly typing in a float instead of int
			self._update_pb(win,self.progressbar_load,5)
			self.comp_list = \
				[int(float(i)) for i in self.compids_var.get().split(',')]
			self._update_pb(win,self.progressbar_load,10)

			# handle gas components separately
			if 0 in self.comp_list:
				if len(self.comp_list) > 1:
					messagebox.showwarning(title='Warning',
					message='Gas + particles still experimental!')
					# self._update_pb(win,self.progressbar_load,0)
					# return
				else:
					self.gas = True

			for _snapfile in self.snaplist_dict:
				_snapobject = \
					rave_core.Snapshot_Processing(snap=_snapfile,
					maxcomp=self.maxcompnum_var.get(),
					complst=self.comp_list,skip=self.skipevery_var.get(),
					verbose=self.debug)
				self._update_pb(win,self.progressbar_load,
					self.progressbar_load['value']+_pbstep)
				# IMPORTANT: Consider calling the following by default
				# within rave_core.py:
				_snapobject.select_particles()
				self._update_pb(win,self.progressbar_load,
					self.progressbar_load['value']+_pbstep)
				if self.com_var.get() == 1:
					_snapobject.com(self.comp_list[0]) # comp_list[0] -> ref. comp.
				self._update_pb(win,self.progressbar_load,
					self.progressbar_load['value']+_pbstep)
				if self.cov_var.get() == 1:
					_snapobject.cov(self.comp_list[0])
				self._update_pb(win,self.progressbar_load,
					self.progressbar_load['value']+_pbstep)
				if self.alignangmom_var.get() == 1:
					_snapobject.alignangmom(self.comp_list[0])
				self.snaplist_dict[_snapfile] = _snapobject

				# IN DEVELOPMENT
				self.famtags_dict_str = self._parse_famtags(win)
				print(self.famtags_dict_str)
				print(self.famtags_dict_map)
				for fam, tag_list in self.famtags_dict_str.items():
					print(fam, tag_list)
					fam_obj = self.famtags_dict_map[fam]
					print(_snapobject.snap_object[fam_obj])

			self._update_pb(win,self.progressbar_load,100)
			# activate control panel plot fields
			self.options_field.config(state='active')
			# report simulation overview
			self._simulation_overview()
			return


	# IN DEVELOPMENT
	def remove_widget(self,widget):
		'''
		Removes a widget
		'''
		save_config = widget.grid_info()
		widget.grid_remove()
		return save_config


	# IN DEVELOPMENT
	def _activate_legacy_selection_fields(self):
		'''
		Activate the legacy selection fields (MaxCompNumber and comp IDs)
		'''
		if self.selectype_var.get(): # button is checked
			self.maxcompnum_label.grid() # reattach, i.e. undo remove
			self.maxcompnum_field.grid()
			self.compids_label.grid()
			self.compids_field.grid()
			self.maxcompnum_field.config(state='active')
			self.compids_field.config(state='active')
			self.remove_widget(self.famtags_label)
			self.remove_widget(self.famtags_field)
		else:
			self.maxcompnum_field.config(state='disabled')
			self.compids_field.config(state='disabled')
			self.remove_widget(self.maxcompnum_label)
			self.remove_widget(self.maxcompnum_field)
			self.remove_widget(self.compids_label)
			self.remove_widget(self.compids_field)
			self.famtags_label.grid()# reattach, i.e. undo remove
			self.famtags_field.grid()


	def _set_load_entries_maxcompnum(self):
		'''
		Sanity checks and warnings on load entries
		'''
		if self.maxcompnum_var.get() != self.MaxCompNumber_default:
			messagebox.showwarning(title='Warning',message='Do not change the value of '+\
				'the parameter MaxCompNumber unless absolutely sure')


	def _set_load_entries_com(self):
		'''
		Sanity checks and warnings on load entries
		Activate/deactivate complementary load entries CoV and Lz||z which
		depend programatically on CoM
		'''
		if self.com_var.get() == 0: # do not allow other options if com is off
			messagebox.showwarning(title='Warning',message='Do not to turn off CoM '+\
				'unless absolutely necessary!')
			self.cov_button.config(state='disable')
			self.alignangmom_button.config(state='disable')
			self.cov_var.set(0)
			self.alignangmom_var.set(0)
		else:
			self.cov_button.config(state='active')
			self.alignangmom_button.config(state='active')
			self.cov_var.set(1)
			self.alignangmom_var.set(1)


	def _set_load_entries_cov(self):
		'''
		Sanity checks and warnings on load entries
		'''
		if self.cov_var.get() == 0 and self.com_var.get() == 1:
			messagebox.showwarning(title='Warning',message='Do not to turn off CoV '+\
				'unless absolutely necessary!')


	def _set_load_entries_angmom(self):
		'''
		Sanity checks and warnings on load entries
		'''
		if self.alignangmom_var.get() == 0 and self.com_var.get() == 1:
			messagebox.showwarning(title='Warning',message='Do not to turn off Lz||z '+\
				'unless absolutely necessary!')


	def _set_custom_plotlabels(self,win=None):
		'''
		Allow user to customize plot (map) labels
		'''
		if self.custom_plotlabels_var.get() == 1:
			win1 = self._popup_window(win,'Custom Plot Labels and Legends')
			# x label
			_row_num_control=0
			self.x_plotlabel_var = tk.StringVar()
			_x_plotlabel = ttk.Label(win1,text='x-axis label:')
			_x_plotlabel_field = ttk.Entry(win1,width=40,
				textvariable=self.x_plotlabel_var)
			_x_plotlabel.grid(row=_row_num_control,column=0,padx=5,pady=4,
				sticky='e')
			_x_plotlabel_field.grid(row=_row_num_control,column=1,padx=5,pady=4)
			# y label
			_row_num_control+=1
			self.y_plotlabel_var = tk.StringVar()
			_y_plotlabel = ttk.Label(win1,text='y-axis label:')
			_y_plotlabel_field = ttk.Entry(win1,width=40,
				textvariable=self.y_plotlabel_var)
			_y_plotlabel.grid(row=_row_num_control,column=0,padx=5,pady=4,
				sticky='e')
			_y_plotlabel_field.grid(row=_row_num_control,column=1,padx=5,pady=4)
			# additional colorbar label
			if self.options_var.get() == 'generic_weighted_map':
				_row_num_control+=1
				self.cb_plotlabel_var = tk.StringVar()
				_cb_plotlabel = ttk.Label(win1,text='colorbar label:')
				_cb_plotlabel_field = ttk.Entry(win1,width=40,
					textvariable=self.cb_plotlabel_var)
				_cb_plotlabel.grid(row=_row_num_control,column=0,padx=5,pady=4,
					sticky='e')
				_cb_plotlabel_field.grid(row=_row_num_control,column=1,padx=5,pady=4)
			# legends; one for each data set
			self.legend_var = []
			for i in range(len(self.snaplist_dict)):
				_row_num_control+=1
				self.legend_var.append(tk.StringVar())
				_plotlegend = ttk.Label(win1,text='legend {}:'.format(i+1))
				_plotlegend_field = ttk.Entry(win1,width=40,
					textvariable=self.legend_var[i])
				_plotlegend.grid(row=_row_num_control,column=0,padx=5,pady=4,
					sticky='e')
				_plotlegend_field.grid(row=_row_num_control,column=1,padx=5,pady=4)
			# fontsize
			_row_num_control+=1
			self.font_size_var = tk.StringVar()
			self.font_size_var.set(12)
			_fontsize = ttk.Label(win1,text='font size:')
			_fontsize_field = ttk.Entry(win1,width=40,
				textvariable=self.font_size_var)
			_fontsize.grid(row=_row_num_control,column=0,padx=5,pady=4,
				sticky='e')
			_fontsize_field.grid(row=_row_num_control,column=1,padx=5,pady=4)
		else:
			return


	def _popup_window(self,win=None,wname='Input Parameters'):
		'''
		Open a temporary, toplevel ('pop-up') window
		that asks for user's input
		'''
		_ws = win.winfo_screenwidth()
		_hs = win.winfo_screenheight()
		win1 = tk.Toplevel(win)
		win1.geometry('+{}+{}'.format(int((_ws/3)),int((_hs/4))))
		win1.title(wname)
		button = ttk.Button(win1,text='Close',command=win1.destroy)
		button.grid(row=6,column=0,columnspan=2,padx=5,pady=4)
		return win1


	def _filter_settings(self,win=None):
		'''
		Request filter parameters from user according to choice
		'''
		# proceed only if a filter is selected
		if self.filter_var.get() == self.filter_list[0]:
			return
		_filter_name = self.filter_var.get()
		if len(_filter_name) < 3: # prevent unnecessary window pop-up
			return
		elif _filter_name == 'All':
			return
		else:
			win1 = self._popup_window(win,_filter_name)
			if _filter_name == 'SolarNeighborhood':
				self._orig_filter_var.set('0,0,0')
				self._r1_filter_var.set(1.)
				self._r2_filter_var.set(2.)
				_o_label = ttk.Label(win1,text='Origin (kpc):')
				_r1_label = ttk.Label(win1,text='Inner radius (kpc):')
				_r2_label = ttk.Label(win1,text='Outter radius (kpc):')
				_o_field = ttk.Entry(win1,textvariable=self._orig_filter_var)
				_r1_field = ttk.Entry(win1,textvariable=self._r1_filter_var)
				_r2_field = ttk.Entry(win1,textvariable=self._r2_filter_var)
				_o_label.grid(row=0,column=0,padx=5,pady=4)
				_o_field.grid(row=0,column=1,padx=5,pady=4)
				_r1_label.grid(row=1,column=0,padx=5,pady=4)
				_r1_field.grid(row=1,column=1,padx=5,pady=4)
				_r2_label.grid(row=2,column=0,padx=5,pady=4)
				_r2_field.grid(row=2,column=1,padx=5,pady=4)
			elif _filter_name == 'Sphere':
				self._orig_filter_var.set('0,0,0')
				self._radius_filter_var.set(1.)
				_o_label = ttk.Label(win1,text='Origin (kpc):')
				_r_label = ttk.Label(win1,text='Radius (kpc):')
				_o_field = ttk.Entry(win1,textvariable=self._orig_filter_var)
				_r_field = ttk.Entry(win1,textvariable=self._radius_filter_var)
				_o_label.grid(row=0,column=0,padx=5,pady=4)
				_o_field.grid(row=0,column=1,padx=5,pady=4)
				_r_label.grid(row=1,column=0,padx=5,pady=4)
				_r_field.grid(row=1,column=1,padx=5,pady=4)
			elif _filter_name == 'Cuboid':
				self._orig_filter_var.set('0,0,0')
				self._side_filter_var.set(1.)
				_o_label = ttk.Label(win1,text='Origin (kpc):')
				_s_label = ttk.Label(win1,text='Length (kpc):')
				_o_field = ttk.Entry(win1,textvariable=self._orig_filter_var)
				_s_field = ttk.Entry(win1,textvariable=self._side_filter_var)
				_o_label.grid(row=0,column=0,padx=5,pady=4)
				_o_field.grid(row=0,column=1,padx=5,pady=4)
				_s_label.grid(row=1,column=0,padx=5,pady=4)
				_s_field.grid(row=1,column=1,padx=5,pady=4)
			elif _filter_name == 'Disc':
				self._orig_filter_var.set('0,0,0')
				self._radius_filter_var.set(1.)
				self._height_filter_var.set(1.)
				_o_label = ttk.Label(win1,text='Origin (kpc):')
				_r_label = ttk.Label(win1,text='Radius (kpc):')
				_z_label = ttk.Label(win1,text='Height (kpc):')
				_o_field = ttk.Entry(win1,textvariable=self._orig_filter_var)
				_r_field = ttk.Entry(win1,textvariable=self._radius_filter_var)
				_z_field = ttk.Entry(win1,textvariable=self._height_filter_var)
				_o_label.grid(row=0,column=0,padx=5,pady=4)
				_o_field.grid(row=0,column=1,padx=5,pady=4)
				_r_label.grid(row=1,column=0,padx=5,pady=4)
				_r_field.grid(row=1,column=1,padx=5,pady=4)
				_z_label.grid(row=2,column=0,padx=5,pady=4)
				_z_field.grid(row=2,column=1,padx=5,pady=4)
			elif _filter_name == 'CircularSector':
				self._orig_filter_var.set('0,0,0')
				self._radius_filter_var.set(1.)
				self._height_filter_var.set(1.)
				self._angle_min_filter_var.set(0.)
				self._angle_max_filter_var.set(360.)
				_o_label = ttk.Label(win1,text='Origin (kpc):')
				_r_label = ttk.Label(win1,text='Radius (kpc):')
				_z_label = ttk.Label(win1,text='Height (kpc):')
				_a1_label = ttk.Label(win1,text='Angle min (deg):')
				_a2_label = ttk.Label(win1,text='Angle max (deg):')
				_o_field = ttk.Entry(win1,textvariable=self._orig_filter_var)
				_r_field = ttk.Entry(win1,textvariable=self._radius_filter_var)
				_z_field = ttk.Entry(win1,textvariable=self._height_filter_var)
				_a1_field = ttk.Entry(win1,
					textvariable=self._angle_min_filter_var)
				_a2_field = ttk.Entry(win1,
					textvariable=self._angle_max_filter_var)
				_o_label.grid(row=0,column=0,padx=5,pady=4)
				_o_field.grid(row=0,column=1,padx=5,pady=4)
				_r_label.grid(row=1,column=0,padx=5,pady=4)
				_r_field.grid(row=1,column=1,padx=5,pady=4)
				_z_label.grid(row=2,column=0,padx=5,pady=4)
				_z_field.grid(row=2,column=1,padx=5,pady=4)
				_a1_label.grid(row=3,column=0,padx=5,pady=4)
				_a1_field.grid(row=3,column=1,padx=5,pady=4)
				_a2_label.grid(row=4,column=0,padx=5,pady=4)
				_a2_field.grid(row=4,column=1,padx=5,pady=4)
			elif _filter_name == 'Arc':
				self._orig_filter_var.set('0,0,0')
				self._r1_filter_var.set(1.)
				self._r2_filter_var.set(2.)
				self._height_filter_var.set(1.)
				self._angle_min_filter_var.set(0.)
				self._angle_max_filter_var.set(360.)
				_o_label = ttk.Label(win1,text='Origin (kpc):')
				_r1_label = ttk.Label(win1,text='Inner radius (kpc):')
				_r2_label = ttk.Label(win1,text='Outter radius (kpc):')
				_z_label = ttk.Label(win1,text='Height (kpc):')
				_a1_label = ttk.Label(win1,text='Angle min (deg):')
				_a2_label = ttk.Label(win1,text='Angle max (deg):')
				_o_field = ttk.Entry(win1,textvariable=self._orig_filter_var)
				_r1_field = ttk.Entry(win1,textvariable=self._r1_filter_var)
				_r2_field = ttk.Entry(win1,textvariable=self._r2_filter_var)
				_z_field = ttk.Entry(win1,textvariable=self._height_filter_var)
				_a1_field = ttk.Entry(win1,
					textvariable=self._angle_min_filter_var)
				_a2_field = ttk.Entry(win1,
					textvariable=self._angle_max_filter_var)
				_o_label.grid(row=0,column=0,padx=5,pady=4)
				_o_field.grid(row=0,column=1,padx=5,pady=4)
				_r1_label.grid(row=1,column=0,padx=5,pady=4)
				_r1_field.grid(row=1,column=1,padx=5,pady=4)
				_r2_label.grid(row=2,column=0,padx=5,pady=4)
				_r2_field.grid(row=2,column=1,padx=5,pady=4)
				_z_label.grid(row=3,column=0,padx=5,pady=4)
				_z_field.grid(row=3,column=1,padx=5,pady=4)
				_a1_label.grid(row=4,column=0,padx=5,pady=4)
				_a1_field.grid(row=4,column=1,padx=5,pady=4)
				_a2_label.grid(row=5,column=0,padx=5,pady=4)
				_a2_field.grid(row=5,column=1,padx=5,pady=4)
			elif _filter_name == 'QuantityFilter':
				self._quantity_filter_var.set('vphi')
				self._quantity_min_filter_var.set(0.)
				self._quantity_max_filter_var.set(300.)
				_q_label = ttk.Label(win1,text='Quantity:')
				_qmin_label = ttk.Label(win1,text='Min. Value:')
				_qmax_label = ttk.Label(win1,text='Max. Value:')
				_q_field = ttk.Entry(win1,textvariable=self._quantity_filter_var)
				_qmin_field = ttk.Entry(win1,textvariable=self._quantity_min_filter_var)
				_qmax_field = ttk.Entry(win1,textvariable=self._quantity_max_filter_var)
				_q_label.grid(row=0,column=0,padx=5,pady=4)
				_q_field.grid(row=0,column=1,padx=5,pady=4)
				_qmin_label.grid(row=1,column=0,padx=5,pady=4)
				_qmin_field.grid(row=1,column=1,padx=5,pady=4)
				_qmax_label.grid(row=2,column=0,padx=5,pady=4)
				_qmax_field.grid(row=2,column=1,padx=5,pady=4)
			else:
				pass


	def _transform_settings(self,win=None):
		'''
		Request transformation parameters from user according to choice
		'''
		# proceed only if a transfromation is selected
		_transform_name = self.transform_var.get()
		if len(_transform_name) < 3: # prevent unnecessary window pop-up
			return
		elif _transform_name == 'None':
			return
		else:
			win1 = self._popup_window(win,_transform_name)
			if _transform_name == 'Rotate':
				self._alpha_transform_var.set(0.)
				self._beta_transform_var.set(0.)
				self._gamma_transform_var.set(0.)
				_alpha_label = ttk.Label(win1,text='Angle around x (deg):')
				_beta_label = ttk.Label(win1,text='Angle around y (deg):')
				_gamma_label = ttk.Label(win1,text='Angle around z (deg):')
				_alpha_field = \
					ttk.Entry(win1,textvariable=self._alpha_transform_var)
				_beta_field = \
					ttk.Entry(win1,textvariable=self._beta_transform_var)
				_gamma_field = \
					ttk.Entry(win1,textvariable=self._gamma_transform_var)
				_alpha_label.grid(row=1,column=0,padx=5,pady=4)
				_alpha_field.grid(row=1,column=1,padx=5,pady=4)
				_beta_label.grid(row=2,column=0,padx=5,pady=4)
				_beta_field.grid(row=2,column=1,padx=5,pady=4)
				_gamma_label.grid(row=3,column=0,padx=5,pady=4)
				_gamma_field.grid(row=3,column=1,padx=5,pady=4)
			else:
				pass

	def _special_cases(self,win=None):
		'''
		Handle variables that require special care
		'''
		_xvar = self.xvalue_var.get()
		_yvar = self.yvalue_var.get()
		_wvar = self.wvalue_var.get()
		_special_vars = \
			('J_R','J_z','J_phi','Omega_z','energy','omega','e_Jacobi','psi')
		if rave_core.np.isin('psi', [_xvar,_yvar,_wvar]):
			msg =' is calculated for all loaded components. It will only be correct if each component ID has its appropriate sign to reflect its shape: discy [+] or spheroidal [-]'
		else:
			msg =f' is calculated for the first component given ({self.comp_list[0]}). It will only be correct if all galaxy components are loaded, with each component ID having its appropriate sign to reflect its shape: discy [+] or spheroidal [-]'
		_varname = None
		if any(q == _xvar for q in _special_vars):
			_varname = _xvar
		elif any(q == _yvar for q in _special_vars):
			_varname = _yvar
		elif any(q == _wvar for q in _special_vars):
			_varname = _wvar
		if _varname is not None:
			messagebox.showwarning(title='Warning', message=f'{_varname}'+msg)
		if rave_core.np.isin('e_Jacobi', [_xvar,_yvar,_wvar]):
			self._set_omega_p(win) # get value from user
		return


	def _set_omega_p(self,win=None):
		'''
		Ask value of pattern speed from user; only relevant when any plot involves e_Jacobi
		'''
		win1 = self._popup_window(win,'Enter pattern speed')
		self.omega_p_var.set(44.0)
		omega_p_label = ttk.Label(win1,text='Omega_p (km/s/kpc):')
		omega_p_field = ttk.Entry(win1,textvariable=self.omega_p_var)
		omega_p_label.grid(row=0,column=0,padx=5,pady=4)
		omega_p_field.grid(row=0,column=1,padx=5,pady=4)
		# win1.wait_window() # VERY IMPORTANT TO BLOCK MAIN LOOP UNTIL CLOSED, NOT NEEDED NOW
		return


	def _set_data_to_plot(self,snap=None,win=None):
		'''
		Set the data to be plotted according to variables chosen by user
		This is necessary because while some quantities can be directly
		calculated for one or all given components (e.g. density map), others
		(e.q. actions) require all components (due to dependence on total
		potential) but they normally are plotted for only one of them (e.g.
		disc). Here the convention is that in such cases, the quantity is
		calculated for the first item in components ID list
		'''
		if snap is None:
			raise ValueError('Provide a snapshot object')
		if win is None:
			raise ValueError('Provide a window object')
		_xvar = self.xvalue_var.get()
		_yvar = self.yvalue_var.get()
		_wvar = self.wvalue_var.get()
		# Special cases (not built in Pynbody; some calculated with AGAMA)
		_special_vars = \
			('J_R','J_z','J_phi','Omega_z','energy','omega','e_Jacobi','psi')
		if any(q == _xvar for q in _special_vars):
			 # use getattr to invoke method to create _xvar array, since snap._xvar won't work:
			getattr(snap,_xvar)()
			sdata = snap.comp_dict[self.comp_list[0]]
		if any(q == _yvar for q in _special_vars):
			getattr(snap,_yvar)()
			sdata = snap.comp_dict[self.comp_list[0]]
		if any(q == _wvar for q in _special_vars):
			if _wvar == 'e_Jacobi':
				getattr(snap,_wvar)(self.omega_p_var.get()) # pass argument
			else:
				getattr(snap,_wvar)()
			sdata = snap.comp_dict[self.comp_list[0]]
		else:
			sdata = snap.compPart_all_skip
		return sdata


	def _apply_filter(self,snap=None,win=None):
		'''
		Select a subset of particles based on geometric constraints
		'''
		if snap is None:
			raise ValueError('Provide a snapshot object')
		if win is None:
			raise ValueError('Provide a window object')
		_filter = self.filter_var.get()
		_sdata = self._set_data_to_plot(snap,win)
		if _filter == 'All':
			_s = _sdata
		elif _filter == 'SolarNeighborhood':
			_origin = \
				tuple([float(i) for i in \
					self._orig_filter_var.get().split(',')])
			_r1 = self._r1_filter_var.get()
			_r2 = self._r2_filter_var.get()
			_s = \
				_sdata[getattr(rave_core.pynbody.filt,
				_filter)(r1=_r1,r2=_r2,cen=_origin)]
		elif _filter == 'Sphere':
			_origin = \
				tuple([float(i) for i in \
					self._orig_filter_var.get().split(',')])
			_radius = self._radius_filter_var.get()
			_s = \
				_sdata[getattr(rave_core.pynbody.filt,
				_filter)(_radius,_origin)]
		elif _filter == 'Cuboid':
			_origin = \
				tuple([float(i) for i in \
					self._orig_filter_var.get().split(',')])
			_sidelength = self._side_filter_var.get()
			_x1 = -0.5*_sidelength+_origin[0]
			_x2 = 0.5*_sidelength+_origin[0]
			_y1 = -0.5*_sidelength+_origin[1]
			_y2 = 0.5*_sidelength+_origin[1]
			_z1 = -0.5*_sidelength+_origin[2]
			_z2 = 0.5*_sidelength+_origin[2]
			_s = \
				_sdata[getattr(rave_core.pynbody.filt,
				_filter)(_x1,_y1,_z1,_x2,_y2,_z2)]
		elif _filter == 'Disc':
			_origin = \
				tuple([float(i) for i in \
					self._orig_filter_var.get().split(',')])
			_radius = self._radius_filter_var.get()
			_height = self._height_filter_var.get()
			_s = _sdata[\
					getattr(rave_core.pynbody.filt,
					_filter)(_radius,_height,_origin) ]
		elif _filter == 'CircularSector':
			_origin = \
				tuple([float(i) for i in \
					self._orig_filter_var.get().split(',')])
			_radius = self._radius_filter_var.get()
			_height = self._height_filter_var.get()
			_angle_min = self._angle_min_filter_var.get()
			_angle_max = self._angle_max_filter_var.get()
			_s = _sdata[\
					getattr(rave_core.pynbody.filt,
					_filter)(_radius,_height,_angle_min,_angle_max,_origin)]
		elif _filter == 'Arc':
			_origin = \
				tuple([float(i) for i in \
					self._orig_filter_var.get().split(',')])
			_r1 = self._r1_filter_var.get()
			_r2 = self._r2_filter_var.get()
			_height = self._height_filter_var.get()
			_angle_min = self._angle_min_filter_var.get()
			_angle_max = self._angle_max_filter_var.get()
			_s = _sdata[\
					getattr(rave_core.pynbody.filt,
					_filter)(_r1,_r2,_height,_angle_min,_angle_max,_origin)]
		elif _filter == 'QuantityFilter': # quantity between (min,max)
			_quant = self._quantity_filter_var.get()
			_min = self._quantity_min_filter_var.get()
			_max =  self._quantity_max_filter_var.get()
			_s = _sdata[\
					getattr(rave_core.pynbody.filt,
					_filter)(_quant,_min,_max)]
		else:
			messagebox.showerror('Filter {} not implemented'.\
				format(_filter))
		# useful information
		print(f'\nParticles within filter selection: {len(_s)}\n')
		return _s

	def _apply_transform(self,snap=None):
		'''
		Apply geomtric transformation as requested by user
		'''
		if snap is None:
			raise ValueError('Provide a snapshot object')
		_transform = self.transform_var.get()
		# make a deep copy to avoid overwriting actual snap object
		_sdata = snap.__deepcopy__()
		if _transform != 'None':
			if _transform == 'Rotate':
				_alpha = self._alpha_transform_var.get()
				_beta = self._beta_transform_var.get()
				_gamma = self._gamma_transform_var.get()
				# apply rotation (note that order matters!!!)
				if abs(_alpha) > 0.:
					_sdata.rotate_x(_alpha)
				if abs(_beta) > 0.:
					_sdata.rotate_y(_beta)
				if abs(_gamma) > 0.:
					_sdata.rotate_z(_gamma)
			else:
				pass
		return _sdata


	def _contour_settings(self,win=None):
		'''
		Request contour parameters from user according to choice
		'''
		win1 = self._popup_window(win,'Contour parameters')
		sigma_label = ttk.Label(win1,text='Sigma:')
		sigma_field = ttk.Entry(win1,textvariable=self.contour_sigma_var)
		levels_label = ttk.Label(win1,text='Levels (csv):')
		levels_field = ttk.Entry(win1,textvariable=self.contour_levels_var)
		color_label = ttk.Label(win1,text='Color:')
		color_field = ttk.Entry(win1,textvariable=self.contour_color_var)
		sigma_label.grid(row=0,column=0,padx=5,pady=4)
		sigma_field.grid(row=0,column=1,padx=5,pady=4)
		levels_label.grid(row=1,column=0,padx=5,pady=4)
		levels_field.grid(row=1,column=1,padx=5,pady=4)
		color_label.grid(row=2,column=0,padx=5,pady=4)
		color_field.grid(row=2,column=1,padx=5,pady=4)


	def _disable_plot_entries(self):
		'''
		Disable plot entries until there is data to plot
		'''
		if self.options_var.get() == self.options_list[0]:
			self.options_field.config(state='disable')
			self._set_weighted_map_entries('disable')
			self._set_contour_map_entries('disable')
			self._set_profile_entries('disable')


	def _set_plot_entries(self,win=None):
		'''
		Activate/deactivate plot entries and set appropriate default values
		depending on plot option chosen
		'''
		if self.options_var.get() == 'generic_weighted_map':
			self._set_profile_entries('disable')
			self._set_contour_map_entries('disable')
			self._set_weighted_map_entries('active')
		elif self.options_var.get() == 'generic_contour_map':
			self._set_profile_entries('disable')
			self._set_weighted_map_entries('disable')
			self._set_contour_map_entries('active')
			self._contour_settings(win)
		elif self.options_var.get() == 'generic_radial_profile':
			self._set_weighted_map_entries('disable')
			self._set_contour_map_entries('disable')
			self._set_profile_entries('active')
			if self.alignangmom_var.get() == 0:
				messagebox.showwarning(title='Warning',message=\
					'If the chosen component is discy, '+\
					'turn on Lz||z for this plot option and reload')
		elif self.options_var.get() == 'generic_3D_scatterplot':
			self._set_profile_entries('disable')
			self._set_contour_map_entries('disable')
			self._set_weighted_map_entries('active')
		else:
			self._set_weighted_map_entries('disable')
			self._set_contour_map_entries('disable')
			self._set_profile_entries('disable')
		# when gas is present:
		if self.gas:
			self.svalue_field.config(state='disabled')


	def _reset_yrange_fields(self,clrplt=0):
		'''
		Reset the dependent variable field values
		'''
		if self.options_var.get() == 'generic_radial_profile' or\
			clrplt == 1:
				self.ymin_var.set(self.ymin_var_default_str)
				self.ymax_var.set(self.ymax_var_default_str)


	def _set_yrange_fields(self,fig=None):
		'''
		Set the dependent variable field values
		'''
		self.ymin_var.set('{:.1E}'.format(fig.get_ylim()[0]))
		self.ymax_var.set('{:.1E}'.format(fig.get_ylim()[1]))


	def _plot(self,win=None):
		'''
		Performs some sanity checks
		Invokes the correct plotting function depending on plot option chosen
		'''
		if len(self.snaplist_dict) < 1 or \
			not any([*self.snaplist_dict.values()]):
				messagebox.showwarning(title='Warning',
					message='No snapshot yet loaded')
		else:
			if self.options_var.get() == self.options_list[0]:
					messagebox.showwarning(title='Warning',
						message='Select an option to plot!')
					self.options_field.focus()
			# elif not self.gas and (self.options_var.get() == self.options_list[1] or
			elif (self.options_var.get() == self.options_list[1] or self.options_var.get() == self.options_list[2]) and self.filter_var.get() == self.filter_list[0]:
					messagebox.showwarning(title='Warning',
						message='Select a filter!')
					self.filter_field.focus()
			else:
				if self.canvas_is_free:
					self._update_pb(win,self.progressbar_plot,0)
					self.options_funcs[self.options_var.get()](win)
				else:
					_reply = messagebox.askyesnocancel(\
						message='Clear canvas first?', default='no')
					if _reply:
						self._clear_plot(win)
					if _reply is not None: # allow user to cancel plot command
						self._update_pb(win,self.progressbar_plot,0)
						self.options_funcs[self.options_var.get()](win)


	def _set_profile_entries(self,state='disabled'):
		'''
		Set entries' state and default values
		'''
		self.xmin_field.config(state=state)
		self.xmin_var.set(0.)
		self.xmax_field.config(state=state)
		self.xscale_field.config(state=state)
		self.xscale_var.set('linear')
		self.ymin_field.config(state=state)
		self.ymin_var.set(self.ymin_var_default_str)
		self.ymax_field.config(state=state)
		self.ymax_var.set(self.ymax_var_default_str)
		self.yscale_field.config(state=state)
		self.yscale_var.set('linear')
		self.resolution_field.config(state=state)
		self.resolution_var.set(self.resolution_var_prof_default)
		self.yvalue_field.config(state=state)
		self.custom_plotlabels_button.config(state=state)
		self.colorbar_button.config(state=state)
		self.colorbar_orientation_button.config(state=state)
		self.lstyle_field.config(state=state)
		self.lcolor_field.config(state=state)
		self.marker_field.config(state=state)


	def _generic_radial_profile_settings(self):
		'''
		Set appropriate variable values depending on selected option
		'''
		if self.lstyle_var.get() == self.lstyle_list[0]:
			messagebox.showwarning(title='Warning',
				message='Select a line style!')
			self.lstyle_field.focus()
			raise ValueError()
			return
		if self.lcolor_var.get() == self.lcolor_list[0]:
			messagebox.showwarning(title='Warning',
				message='Select a line color!')
			self.lcolor_field.focus()
			raise ValueError()
			return
		if self.marker_var.get() == self.marker_list[0]:
			messagebox.showwarning(title='Warning',
				message='Select a marker!')
			self.marker_field.focus()
			raise ValueError()
			return
		xlab = 'R (kpc)'
		_choice = self.yvalue_var.get()
		if _choice == self.yvalue_list[0]:
			messagebox.showwarning(title='Warning',
				message='Select a y-value!')
			self.yvalue_field.focus()
		elif _choice == 'surfdensity':
			ylab = 'Density'
			plab = '$\Sigma(R)$ (Msun kpc$^{-2}$)'
			var = 'density'
			scale = 'lin' #'log'
			dim = 2
		elif _choice == 'voldensity':
			ylab = 'Density'
			plab = '$\\rho(r)$ (Msun kpc$^{-3}$)'
			var = 'density'
			scale = 'log'
			dim = 3
		elif _choice == 'vel_R_disp':
			ylab = 'Speed (km s$^{-1}$)'
			plab = '$\sigma_{vR}$'
			var = 'vrxy_disp'
			scale = 'lin'
			dim = 2
		elif _choice == 'vel_z_disp':
			ylab = 'Speed (km s$^{-1}$)'
			plab = '$\sigma_{vz}$'
			var = 'vz_disp'
			scale = 'lin'
			dim = 2
		elif _choice == 'v_circ':
			ylab = 'Speed (km s$^{-1}$)'
			plab = '$v_c$'
			var = 'v_circ'
			scale = 'log'
			dim = 2
		elif _choice == 'omega':
			messagebox.showwarning(title='Warning', message=\
				f'{_choice} will only be correct if all galaxy '+\
				'components are loaded!')
			ylab = 'Frequency (km s$^{-1}$ kpc$^{-1}$)'
			plab = '$\Omega$'
			var = 'omega'
			scale = 'log'
			dim = 2
		elif _choice == 'kappa':
			messagebox.showwarning(title='Warning', message=\
				f'{_choice} will only be correct if all galaxy '+\
				'components are loaded!')
			ylab = 'Frequency (km s$^{-1}$ kpc$^{-1}$)'
			plab = '$\kappa$'
			var = 'kappa'
			scale = 'log'
			dim = 2
		elif _choice == 'Omega_z':
			messagebox.showwarning(title='Warning', message=\
				f'{_choice} is calculated for the first component given. '+\
				'It will only be correct if all galaxy '+\
				'components are loaded, with each component ID '+\
				'having its appropriate sign!')
			ylab = 'Frequency (km s$^{-1}$ kpc$^{-1}$)'
			plab = '$\Omega_z$'
			var = 'Omega_z'
			scale = 'log'
			dim = 2
		elif _choice == 'vphi':
			ylab = 'Speed (km s$^{-1}$)'
			plab = '$v_{\phi}$'
			var = 'vphi'
			scale = 'log'
			dim = 2
		elif _choice == 'vrxy':
			ylab = 'Speed (km s$^{-1}$)'
			plab = '$v_{R}$'
			var = 'vrxy'
			scale = 'log'
			dim = 2
		elif _choice == 'vx':
			ylab = 'Speed (km s$^{-1}$)'
			plab = '$v_{x}$'
			var = 'vx'
			scale = 'log'
			dim = 2
		elif _choice == 'vy':
			ylab = 'Speed (km s$^{-1}$)'
			plab = '$v_{y}$'
			var = 'vy'
			scale = 'log'
			dim = 2
		elif _choice == 'vz':
			ylab = 'Speed (km s$^{-1}$)'
			plab = '$v_{z}$'
			var = 'vz'
			scale = 'log'
			dim = 2
		elif rave_core.np.isin(_choice, list('xyz')):
			ylab = f'{_choice} (kpc)'
			plab = _choice
			var = _choice
			scale = 'lin' #'log'
			dim = 2
		elif _choice == 'jz': # not to be confused with the vertical action J_Z!
			ylab = 'Specific angular momentum (kpc km s$^{-1}$)'
			plab = '$j_{z}$'
			var = 'jz'
			scale = 'lin'
			dim = 2
		elif _choice == 'Toomre_Q_star':
			messagebox.showwarning(title='Warning', message=\
				'This quantity is calculated for the first component given. '+\
				'It will only be correct if all galaxy '+\
				'components are loaded.')
			ylab = 'Toomre\'s Q (*)'
			plab = 'Q(R)'
			var = 'Toomre_Q_star'
			scale = 'log'
			dim = 2
		elif _choice == 'Toomre_Q_gas':
			messagebox.showwarning(title='Warning', message=\
				'This quantity is calculated for the first component given. '+\
				'It will only be correct if all galaxy '+\
				'components are loaded.')
			ylab = 'Toomre\'s Q (gas)'
			plab = 'Q(R)'
			var = 'Toomre_Q_gas'
			scale = 'log'
			dim = 2
		elif _choice == 'mass':
			ylab = 'Mass (Msun)'
			plab = '$M(R)$'
			var = 'mass'
			scale = 'log'
			dim = 3
		elif _choice == 'mass_enc':
			ylab = 'Mass (Msun)'
			plab = '$M(<R)$'
			var = 'mass_enc'
			scale = 'log'
			dim = 3
		elif _choice == 'pot':
			ylab = 'Total Potential (km$^{2}$ s$^{-2}$)'
			plab = '$\Phi(R)$'
			var = 'pot'
			scale = 'log'
			dim = 2
		elif _choice == 'temp':
			ylab = 'Gas Temperature (K)'
			plab = '$T_{gas}(R)$'
			var = 'temp'
			scale = 'log'
			dim = 2
		elif _choice == 'p':
			ylab = 'Gas Pressure ($g$ cm$^{-3}$)'
			plab = '$P_{gas}(R)$'
			var = 'p'
			scale = 'log'
			dim = 2
		else: # generic profile
			# messagebox.showerror(message='Profile not implemented: {}'.\
			# 	format(_choice))
			# raise ValueError()
			# return
			ylab = _choice
			plab = _choice
			var = _choice
			scale = 'lin' # may not be adequate
			dim = 2 # may not be adequate

		# overwrite default labels with user input:
		if self.custom_plotlabels_var.get() == 1:
			if len(self.x_plotlabel_var.get()) > 0:
				xlab = self.x_plotlabel_var.get()
			if len(self.y_plotlabel_var.get()) > 0:
				ylab = self.y_plotlabel_var.get()

		return xlab, ylab, plab, var, scale, dim


	# scale functions
	def squareroot(self,x):
		return abs(x)**0.5 # avoid negative values

	def squared(self,x):
		return x**2

	def abs(self,x):
		return abs(x)

	def sign(self,x):
		return x*rave_core.np.sign(x)

	def xforward(self,x):
		'''
		Set the appropriate forward function to scale x axis
		See also inverse()
		'''
		if self.xscale_var.get() == 'squareroot':
			func = self.squareroot
		if self.xscale_var.get() == 'squared':
			func = self.squared
		if self.xscale_var.get() == 'abs':
			func = self.abs
		return func(x)

	def xinverse(self,x):
		'''
		Set the appropriate inverse function to scale x axis
		See also forward()
		'''
		if self.xscale_var.get() == 'squareroot':
			func = self.squared
		if self.xscale_var.get() == 'squared':
			func = self.squareroot
		if self.xscale_var.get() == 'abs':
			func = self.sign
		return func(x)

	def yforward(self,y):
		'''
		Set the appropriate forward function to scale y axis
		See also inverse()
		'''
		if self.yscale_var.get() == 'squareroot':
			func = self.squareroot
		if self.yscale_var.get() == 'squared':
			func = self.squared
		if self.yscale_var.get() == 'abs':
			func = self.abs
		return func(y)

	def yinverse(self,y):
		'''
		Set the appropriate inverse function to scale y axis
		See also forward()
		'''
		if self.yscale_var.get() == 'squareroot':
			func = self.squared
		if self.yscale_var.get() == 'squared':
			func = self.squareroot
		if self.yscale_var.get() == 'abs':
			func = self.sign
		return func(y)

	def _plot_generic_radial_profile(self,win=None):
		'''
		Creates a 1D profile of the particle's selected quantity
		'''
		try:
			_xlabel, _ylabel, _label, _var, _scale, _dim = \
				self._generic_radial_profile_settings()
		except:
			return
		_minR = self.xmin_var.get()
		_pltrng = self.xmax_var.get()
		_resol = self.resolution_var.get()
		self._update_pb(win,self.progressbar_plot,10)
		self._panel1.set_xlabel(_xlabel)
		self._panel1.set_ylabel(_ylabel)
		_xscale = self.xscale_var.get()
		_yscale = self.yscale_var.get()
		if _xscale=='linear' or _xscale=='log': #built-in
			self._panel1.set_xscale(_xscale)
		else:									#custom
			self._panel1.set_xscale('function',
				functions=(self.xforward,self.xinverse))
		if _yscale=='linear' or _yscale=='log': #built-in
			self._panel1.set_yscale(_yscale)
		else:									#custom
			self._panel1.set_yscale('function',
				functions=(self.yforward,self.yinverse))
		if self.ymin_var.get() != self.ymin_var_default_str:
			self._panel1.set_ylim(bottom=float(self.ymin_var.get()))
		if self.ymax_var.get() != self.ymax_var_default_str:
			self._panel1.set_ylim(top=float(self.ymax_var.get()))
		self._update_pb(win,self.progressbar_plot,30)
		indx = 0
		for _filen, _snapobject in self.snaplist_dict.items():
			_x, _y = \
			_snapobject.profile(_var,_resol,_pltrng,_scale,_dim,_minR)
			# store data for saving to file
			self.xytable = [(i,j) for i,j in zip(_x,_y)]
			self._update_pb(win,self.progressbar_plot,50)
			# overwrite default label with user choices
			_labeladd = os.path.basename(_filen)
			_full_label=_label+'; '+_labeladd
			try:
				self.legend_var
				if len(self.legend_var[indx].get()) > 0:
					_full_label=self.legend_var[indx].get()
			except:
				pass
			_mrk = self.marker_var.get()
			_ls = self.lstyle_var.get()
			_col = self.lcolor_var.get()
			if _full_label.lower() == 'none':
				self._panel1.plot(_x,_y,
					linestyle=_ls,color=_col,marker=_mrk)
			else:
				self._panel1.plot(_x,_y,
					linestyle=_ls,color=_col,marker=_mrk,
					label=_full_label)
			indx+=1
		self._update_pb(win,self.progressbar_plot,75)
		self._panel1.legend(frameon=self._boxed_legend)
		self.figure.tight_layout()
		self.canvas.draw()
		self._update_pb(win,self.progressbar_plot,100)
		self.canvas_is_free = False


	def _set_weighted_map_entries(self,state='disabled'):
		'''
		Set entries' state and default values
		'''
		self.xvalue_field.config(state=state)
		self.yvalue_field.config(state=state)
		self.wvalue_field.config(state=state)
		self.svalue_field.config(state=state)
		if state == 'active':
			self.filter_field.config(state='readonly')
			# if not self.gas:
			# 	self.filter_field.config(state='readonly')
			# else:
			# 	self.filter_var.set(self.filter_list[1])
		else:
			self.filter_field.config(state=state)
		self.transform_field.config(state=state)
		self.xmin_field.config(state=state)
		self.xmax_field.config(state=state)
		self.xscale_field.config(state=state)
		self.ymin_field.config(state=state)
		self.ymax_field.config(state=state)
		self.yscale_field.config(state=state)
		self.aspect_field.config(state=state)
		self.resolution_field.config(state=state)
		self.resolution_var.set(self.resolution_var_map_default)
		self.cbmin_field.config(state=state)
		self.cbmax_field.config(state=state)
		self.cbscale_field.config(state=state)
		self.colormaps_field.config(state=state)
		self.xmin_var.set(self.xmin_var_default)
		self.xmax_var.set(self.xmax_var_default)
		self.ymin_var.set(self.ymin_var_default)
		self.ymax_var.set(self.ymax_var_default)
		self.xscale_var.set('linear')
		self.yscale_var.set('linear')
		self.custom_plotlabels_button.config(state=state)
		self.colorbar_button.config(state=state)
		self.colorbar_orientation_button.config(state=state)
		self.interpol_field.config(state=state)


	def _set_contour_map_entries(self,state='disabled'):
		'''
		Set entries' state and default values
		'''
		self.xvalue_field.config(state=state)
		self.yvalue_field.config(state=state)
		self.wvalue_field.config(state=state)
		self.svalue_field.config(state=state)
		if state == 'active':
			self.filter_field.config(state='readonly')
			# if not self.gas:
			# 	self.filter_field.config(state='readonly')
			# else:
			# 	self.filter_var.set(self.filter_list[1])
		else:
			self.filter_field.config(state=state)
		self.transform_field.config(state=state)
		self.xmin_field.config(state=state)
		self.xmax_field.config(state=state)
		self.xscale_field.config(state=state)
		self.ymin_field.config(state=state)
		self.ymax_field.config(state=state)
		self.yscale_field.config(state=state)
		self.aspect_field.config(state=state)
		self.resolution_field.config(state=state)
		self.resolution_var.set(self.resolution_var_map_default)
		self.cbmin_field.config(state=state)
		self.cbmax_field.config(state=state)
		self.cbscale_field.config(state=state)
		self.xmin_var.set(self.xmin_var_default)
		self.xmax_var.set(self.xmax_var_default)
		self.ymin_var.set(self.ymin_var_default)
		self.ymax_var.set(self.ymax_var_default)
		self.xscale_var.set('linear')
		self.yscale_var.set('linear')
		self.custom_plotlabels_button.config(state=state)
		self.colorbar_button.config(state=state)
		self.colorbar_orientation_button.config(state=state)
		self.interpol_field.config(state=state)
		return


	def _plot_generic_3D_scatterplot(self,win=None):
		'''
		Creates a pseudo 3D view of the particles positions
		'''
		if win is None:
			raise ValueError('Provide a window object')
		# gather data
		_filen, _snapobject = list(self.snaplist_dict.items())[0] #1st item only
		try:
			_snap_filt = self._apply_filter(_snapobject,win)
		except:
			messagebox.showwarning(message=f'Could not apply filter: {self.filter_var.get()}')
			return
		try:
			_snap_filt = self._apply_transform(_snap_filt)
		except:
			messagebox.showwarning(message=f'Could not apply transformation: {self.transform_var.get()}')
			return
		_x = _snap_filt[self.xvalue_var.get()]
		_y = _snap_filt[self.yvalue_var.get()]
		_z = _snap_filt['z']
		# set plot labels
		_xlabel = self.xvalue_var.get()
		_ylabel = self.yvalue_var.get()
		_zlabel = 'z (kpc)'
		_fontsize = self.default_fontsize
		# overwrite axes to handle 3D projection:
		_panel1 = self.figure.add_subplot(1,1,1,projection='3d')
		_panel1.set_xlabel(_xlabel,fontsize=_fontsize)
		_panel1.set_ylabel(_ylabel,fontsize=_fontsize)
		_panel1.set_zlabel(_zlabel,fontsize=_fontsize)
		_panel1.scatter(_x,_y,_z,
			marker='.', # use a point marker (pixel , does not work)
			s=(72./self.figure.dpi)**2) # of size one pixel
		_panel1.view_init(0,0)
		_panel1.set_box_aspect((4,4,1)) # aspect ratio x:y:z
		self.canvas.draw()
		self._update_pb(win,self.progressbar_plot,100)
		self.canvas_is_free = False

		return

	def _plot_generic_weighted_map(self,win=None):
		'''
		Creates a 2D projection of the weighted particle density on canvas
		See rave_core.generic_weighted_map for more info
		'''
		# Perform some sanity checks on user's settings
		if win is None:
			raise ValueError('Provide a window object')
		if len(self.snaplist_dict) > 1:
			messagebox.showwarning(title='Warning', message= \
				'This plot option should generally be used with '+ \
				'one snaphsot only')
		elif self.colormaps_var.get() == self.colormaps_list[0]:
			messagebox.showwarning(title='Warning',
				message='Select a color scheme!')
		elif self.xvalue_var.get() == self.xvalue_list[0] or \
			self.yvalue_var.get() == self.yvalue_list[0] or \
			self.wvalue_var.get() == self.wvalue_list[0]:
				messagebox.showwarning(title='Warning',
					message='Select an x- and y-value, '+ \
				'a field, and a statistic (optional)')
				self.xvalue_field.focus()
		else:
			self._update_pb(win,self.progressbar_plot,10)
			_xmin = self.xmin_var.get()
			_xmax = self.xmax_var.get()
			_ymin = float(self.ymin_var.get())
			_ymax = float(self.ymax_var.get())
			_resol = self.resolution_var.get()
			_filen, _snapobject = list(self.snaplist_dict.items())[0] #1 item only
			try:
				_snap_filt = self._apply_filter(_snapobject,win)
			except:
				messagebox.showwarning(message=f'Could not apply filter: {self.filter_var.get()}')
				return
			try:
				_snap_filt = self._apply_transform(_snap_filt)
			except:
				messagebox.showwarning(message=f'Could not apply transformation: {self.transform_var.get()}')
				return
			self._update_pb(win,self.progressbar_plot,20)
			# gather xy-data
			_x = _snap_filt[self.xvalue_var.get()]
			_y = _snap_filt[self.yvalue_var.get()]
			# if self.wvalue_var.get() == 'uniform':
			# 	_w = rave_core.np.ones(len(_x))
			# else:
			# 	_w = _snap_filt[self.wvalue_var.get()]
			# _stat = self.svalue_var.get()
			# set plot labels
			if self.custom_plotlabels_var.get() == 1:
				_xlabel = self.x_plotlabel_var.get()
				_ylabel = self.y_plotlabel_var.get()
				_cblabel = self.cb_plotlabel_var.get()
				_fontsize = self.font_size_var.get()
				matplotlib.rcParams.update({'font.size':_fontsize})
			else:
				_fontsize = self.default_fontsize
				_wlabel = self.wvalue_var.get()
				_xlabel = self.xvalue_var.get()
				_ylabel = self.yvalue_var.get()
				# HERE: add other cases
				if rave_core.np.isin(_xlabel, ['x', 'y', 'z']):
					_xlabel += ' (kpc)'
				if rave_core.np.isin(_ylabel, ['x', 'y', 'z']):
					_ylabel += ' (kpc)'
				# Alternatively: let the generic_weighted_map() return the appropriate units
				if not self.gas: # collisionless components (gas components are handled further down below)
					if _wlabel == 'mass':
						_wlabel = 'density'
						_mapUnits_latex = 'Msun kpc$^{-2}$'
					elif _wlabel == 'level':
						_wlabel = 'AMR level'
						_mapUnits_latex = ''
					elif rave_core.np.isin(_wlabel, ['x', 'y', 'z']):
						_wlabel = f'${_wlabel}$'
						_mapUnits_latex = 'kpc'
					elif rave_core.np.isin(_wlabel, ['vx', 'vy', 'vz']):
						_wlabel = f'${_wlabel[0]}_{_wlabel[1]}$'
						_mapUnits_latex = 'km s$^{-1}$'
					elif _wlabel == 'vrxy':
						_wlabel = '$v_R$'
						_mapUnits_latex = 'km s$^{-1}$'
					elif _wlabel == 'vphi':
						_wlabel = '$v_\phi$'
						_mapUnits_latex = 'km s$^{-1}$'
					else:
						messagebox.showwarning(title='Warning',
							message=f'Setting dummy units for projected variable {_wlabel}')
						_mapUnits_latex = ''
			self._update_pb(win,self.progressbar_plot,30)
			# create map
			if not self.gas: # collisionless components handled by Pynbody
				# define map weight
				if self.wvalue_var.get() == 'uniform':
					_w = rave_core.np.ones(len(_x))
				else:
					_w = _snap_filt[self.wvalue_var.get()]
				_stat = self.svalue_var.get()
				_histo_object = \
					_snapobject.generic_weighted_map(_x,_y,_w,
					_resol,_xmin,_xmax,_ymin,_ymax,_stat)
				_matrix1 = _histo_object.statistic
			if self.gas: # gas components handled both by Pynbody and by YT
			# the reason is that Pynbody does not provide some hydro-variables, e.g. AMR level, or does not work well with some (e.g. temperature), while YT yields weird results for some (e.g. vrxy, vphi). The projected density is handled well by either, but I'm using YT for backwards compatibility
				_filen, _ = list(self.snaplist_dict.items())[0] # 1 item only
				if _xlabel[0] == 'x':
					if _ylabel[0] == 'y':
						projAxis=2
					else:
						projAxis=1
				elif _xlabel[0] == 'y':
					if _ylabel[0] == 'z':
						projAxis=0
					else:
						messagebox.showwarning(title='Warning',
							message='Not an appropriate projection')
						return
				elif _xlabel[0] == 'z':
						messagebox.showwarning(title='Warning',
							message='Not a valid projection!')
						return
				_hydroFactor = 1. # default conversion factor
				_projFactor = 1. # needed to mirror y-quantities on xz projections
				# match Pynbody variables names to YT names:
				if _wlabel == 'mass':
					# I go back and forth between these two...
					# YT settings (use for backwards compatibility):
					# _hydroVar_name = 'density'
					# _mapUnits_latex = 'Msun kpc$^{-2}$'
					# _plot_backend = 'YT'
					# Pynbody settings:
					_hydroVar_name = 'rho'
					_wlabel = 'density'
					_mapUnits_latex = 'Msun kpc$^{-2}$'
					_mapUnits = "Msol kpc^-2 "
					_av_z = False # no need for density-weighted avg along los
					_plot_backend = 'Pynbody'
				elif _wlabel == 'tracer':
					_hydroVar_name = 'tracer'
					_wlabel = 'Colour Tracer'
					_mapUnits_latex = ''
					_mapUnits = None
					_av_z = 'rho' # density-weighted average along sigthline
					_plot_backend = 'Pynbody'
				elif _wlabel == 'temp':
					_hydroVar_name = 'temperature'
					_wlabel = 'temperature'
					_mapUnits_latex = 'K'
					_plot_backend = 'YT'
				elif _wlabel == 'level':
					_hydroVar_name = 'grid_level'
					_wlabel = 'AMR level'
					_mapUnits_latex = ''
					_plot_backend = 'YT'
				elif _wlabel == 'p':
					# YT settings:
					_hydroVar_name = 'pressure'
					_wlabel = 'pressure'
					_mapUnits_latex = 'cm$^{-3}$ K'
					# Boltzmann constant over atomic mass:
					_hydroFactor = \
						rave_core.units.k.in_units('erg K^-1') / rave_core.units.m_p.in_units('g')
					_plot_backend = 'YT'
					# Pynbody settings:
					# _mapUnits = "km^2 Msol s^-2 kpc^-3"
					# _mapUnits_latex = "km^2 Msol s^-2 kpc^-3"
					# _av_z = 'rho' # density-weighted average along sigthline
					# _plot_backend = 'Pynbody'
				# NOTE: When _plot_backend = 'Pynbody', projecting vectorial quantities has to be done with care due to the rotation involved (see below):
				elif rave_core.np.isin(_wlabel, ['x', 'y', 'z']):
					_hydroVar_name = _wlabel
					_wlabel = f'${_wlabel}$'
					_mapUnits_latex = 'kpc'
					_mapUnits = 'kpc'
					_av_z = 'rho' # density-weighted average along sigthline
					_plot_backend = 'Pynbody'
				elif _wlabel == 'vx':
					if projAxis == 0: # yz projection
						_hydroVar_name = 'vz'
					else: # also valid for projAxis = 1 <-> xz projection
						_hydroVar_name = 'vx'
					_wlabel = '$v_x$'
					_mapUnits_latex = 'km s$^{-1}$'
					_mapUnits = 'km s^-1'
					_av_z = 'rho' # density-weighted average along sigthline
					_plot_backend = 'Pynbody'
				elif _wlabel == 'vy':
					if projAxis == 0: # yz projection
						_hydroVar_name = 'vx'
					elif projAxis == 1: # xz projection
						_hydroVar_name = 'vz'
						_projFactor = -1. # mirror y
					else:
						_hydroVar_name = 'vy'
					_wlabel = '$v_y$'
					_mapUnits_latex = 'km s$^{-1}$'
					_mapUnits = 'km s^-1'
					_av_z = 'rho' # density-weighted average along sigthline
					_plot_backend = 'Pynbody'
				elif _wlabel == 'vz':
					if projAxis == 0: # yz projection
						_hydroVar_name = 'vy'
					elif projAxis == 1: # xz projection
						_hydroVar_name = 'vy'
					else:
						_hydroVar_name = 'vz'
					_wlabel = '$v_z$'
					_mapUnits_latex = 'km s$^{-1}$'
					_mapUnits = 'km s^-1'
					_av_z = 'rho' # density-weighted average along sigthline
					_plot_backend = 'Pynbody'
				elif _wlabel == 'vrxy':
					# # YT settings
					# _hydroVar_name = 'velocity_cylindrical_radius'
					# _wlabel = '$v_R$'
					# _mapUnits_latex = 'km s$^{-1}$'
					# _plot_backend = 'YT'
					# Pynbody settings
					_hydroVar_name = 'vrxy'
					_wlabel = '$v_R$'
					_mapUnits_latex = 'km s$^{-1}$'
					_mapUnits = 'km s^-1'
					_av_z = 'rho' # density-weighted average along sigthline
					_plot_backend = 'Pynbody'
				elif _wlabel == 'vphi':
					# # YT settings
					# _hydroVar_name = 'velocity_cylindrical_theta'
					# _wlabel = '$v_R$'
					# _mapUnits_latex = 'km s$^{-1}$'
					# _plot_backend = 'YT'
					# Pynbody settings
					_hydroVar_name = 'vphi'
					_wlabel = '$v_\phi$'
					_mapUnits_latex = 'km s$^{-1}$'
					_mapUnits = 'km s^-1'
					_av_z = 'rho' # density-weighted average along sigthline
					_plot_backend = 'Pynbody'
				else:
					messagebox.showwarning(title='Warning',
						message=f'Hydro variable {_wlabel} not yet implemented!')
					return
				if _plot_backend == 'Pynbody':
					# rotate to desired projection (won't affect original snapshot)
					# make a deep copy to left original snapshot untouched:
					_snap_deepcopy = _snap_filt.__deepcopy__()
					# print(_snap_deepcopy is _snap_filt) # False
					if projAxis == 0: # yz projection
						# rotate clockwise around z-axis such that +y-axis points towards +x-axis, and old +x-axis towards -y-axis
						_snap_deepcopy.rotate_z(-90.)
						# rotate clockwise around new x-axis such old +x-axis points towards +z-axis
						_snap_deepcopy.rotate_x(-90.)
					if projAxis == 1: # xz projection
						# rotate anti-clockwise around x-axis such that +y-axis points towards +z-axis
						_snap_deepcopy.rotate_x(-90.)
					if projAxis == 2:  # xy projection
						# no need to rotate
						pass
					_width = max(abs(el) for el in [_xmin, _xmax, _ymin, _ymax])
					print(f'pynbody.plot.sph.image can only handle square maps! Will set width to: {2*_width}')
					_matrix1 = \
						rave_core.pynbody.plot.sph.image(_snap_deepcopy.gas,
						qty=_hydroVar_name,
						av_z=_av_z, # density weighted average
						units=_mapUnits,
						width=2*_width, # from left to right, bottom to top
						noplot=True,
						resolution=_resol)
					del _snap_deepcopy # free memory
				if _plot_backend == 'YT':
					_matrix1 = \
						yt_utils.\
							weighted_map(_filen,
								_hydroVar_name,
								[_xmin,_xmax,_ymin,_ymax],
								projAxis,
								_resol)
				# rescale map (if required)
				_matrix1 *= _hydroFactor
				# mirror map (if required)
				_matrix1 *= _projFactor
			# convert to absolute value (if required)
			if self.cbscale_var.get() == 'abs_log' or self.cbscale_var.get() == 'abs':
				_matrix1 = rave_core.np.abs(_matrix1)
			# store data for saving to file
			self.xymap = _matrix1
			_cbmin = self.cbmin_var.get()
			_cbmax = self.cbmax_var.get()
			# _cmap=getattr(matplotlib.cm,self.colormaps_var.get())
			_cmap=self.colormaps_var.get()
# 			_cmap = matplotlib.cm.get_cmap('bwr',3)# discrete colorbar, 3 levels
			if _wlabel == 'uniform':
				_cblabel = f'Projected density ({_mapUnits_latex})'
			else:
				if not self.gas:
					if _wlabel == 'mass':
						_cblabel = f'Projected {_wlabel} density' + ' (Msun kpc$^{-2}$)'
					else:
						_cblabel = f'Projected {_wlabel} [{_stat}] weighted by mass ({_mapUnits_latex})'
				else:
					if _wlabel == 'density':
						_cblabel = f'Projected mass density ({_mapUnits_latex})'
					else:
						_cblabel = f'Projected {_wlabel} weighted by gas mass ({_mapUnits_latex})'

			if self.cbscale_var.get() == 'log' or self.cbscale_var.get() == 'abs_log':
				_norm = mcolors.LogNorm(vmin=_cbmin,vmax=_cbmax)
				_tics_form = LogFormatterExponent(10,labelOnlyBase=False)
			else:
				_norm = mcolors.Normalize(vmin=_cbmin,vmax=_cbmax)
				_tics_form = None # i.e. default
			_extents = (_xmin,_xmax,_ymin,_ymax)
			# ensure square plots for equal axes ranges
			# if abs(_xmin) == abs(_xmax) and _xmin == _ymin\
			# 	and abs(_ymin) == abs(_ymax):
			# 		_aspect = 'equal'
			# else:
			_aspect = self.aspect_var.get()
				# _aspect = abs(_ymax - _ymin) / abs(_xmax - _xmin)
				# _aspect = 'auto'
			self._update_pb(win,self.progressbar_plot,50)
			# Take care of imshow interpolation; the difference between
			# image sampling and display resolution may lead to aliasing
			# effects
			# From: https://matplotlib.org/3.2.1/gallery/->
			#->images_contours_and_fields/interpolation_methods.html
			# 'For the Agg, ps and pdf backends, interpolation = 'none'
			# works well when a big image is scaled down, while 'nearest' works
			# well when a small image is scaled up.'
			# Usually, 'nearest' works best
			self._update_pb(win,self.progressbar_plot,75)
			self._panel1.set_xlabel(_xlabel,fontsize=_fontsize)
			self._panel1.set_ylabel(_ylabel,fontsize=_fontsize)
			self._panel1.tick_params(labelsize=_fontsize)
			_img1 = self._panel1.imshow(_matrix1,
				interpolation=self.interpol_var.get(),
				norm=_norm,cmap=_cmap,
				origin='lower',
				extent=_extents)
			try:
				_legend = self.legend_var[0].get()
			except:
				_legend = os.path.basename(_filen)
			self._panel1.text(0.95,0.95,_legend,
				horizontalalignment='right',
				verticalalignment='top',
				transform=self._panel1.transAxes,
				bbox={'facecolor': 'white', 'pad': 4},
				fontsize=_fontsize)
			if self.colorbar_var.get() == 1:
				if self.colorbar_orientation_var.get() == 0:
					_orientation='horizontal'
				else:
					_orientation='vertical'
				cb = self.figure.colorbar(_img1,ax=self._panel1,
					format=_tics_form,label=_cblabel,orientation=_orientation)
			self._panel1.set_aspect(_aspect)
			self.canvas.draw()
			self._update_pb(win,self.progressbar_plot,100)
			self.canvas_is_free = False
			cb = None


	def _plot_generic_contour_map(self,win=None):
		'''
		Creates contours of the 2D projection of the weighted particle
		density on canvas
		See rave_core.generic_weighted_map for more info
		'''
		# Perform some sanity checks on user's settings
		if len(self.snaplist_dict) > 1:
			messagebox.showwarning(title='Warning',
				message=\
				'This plot option should generally be used with '+\
				'one snaphsot only')
		elif self.xvalue_var.get() == self.xvalue_list[0] or\
			self.yvalue_var.get() == self.yvalue_list[0] or\
			self.wvalue_var.get() == self.wvalue_list[0]:
				messagebox.showwarning(title='Warning',
					message='Select an x- and y-value, '+\
				'a field, and a statistic (optional)')
				self.xvalue_field.focus()
		else:
			self._update_pb(win,self.progressbar_plot,10)
			_xmin = self.xmin_var.get()
			_xmax = self.xmax_var.get()
			_ymin = float(self.ymin_var.get())
			_ymax = float(self.ymax_var.get())
			_resol = self.resolution_var.get()
			_filen, _snapobject = list(self.snaplist_dict.items())[0] #1st item only
			try:
				_snap_filt = self._apply_filter(_snapobject,win)
			except:
				messagebox.showwarning(message=f'Could not apply filter: {self.filter_var.get()}')
				return
			try:
				_snap_filt = self._apply_transform(_snap_filt)
			except:
				messagebox.showwarning(message=f'Could not apply transformation: {self.transform_var.get()}')
				return
			self._update_pb(win,self.progressbar_plot,20)
			# gather xy-data
			_x = _snap_filt[self.xvalue_var.get()]
			_y = _snap_filt[self.yvalue_var.get()]
			# if self.wvalue_var.get() == 'uniform':
			# 	_w = rave_core.np.ones(len(_x))
			# else:
			# 	_w = _snap_filt[self.wvalue_var.get()]
			# _stat = self.svalue_var.get()
			# set plot labels
			if self.custom_plotlabels_var.get() == 1:
				_xlabel = self.x_plotlabel_var.get()
				_ylabel = self.y_plotlabel_var.get()
			else:
				_xlabel = self.xvalue_var.get()
				_ylabel = self.yvalue_var.get()
				if _xlabel == 'x' or _xlabel == 'y' or _xlabel == 'z':
					_xlabel += ' (kpc)'
				if _ylabel == 'x' or _ylabel == 'y' or _ylabel == 'z':
					_ylabel += ' (kpc)'
			self._update_pb(win,self.progressbar_plot,30)
			# create map
			if not self.gas: # collisionless components handled by Pynbody
				# define map weight
				if self.wvalue_var.get() == 'uniform':
					_w = rave_core.np.ones(len(_x))
				else:
					_w = _snap_filt[self.wvalue_var.get()]
				_stat = self.svalue_var.get()
				_histo_object = \
					_snapobject.generic_weighted_map(_x,_y,_w,
					_resol,_xmin,_xmax,_ymin,_ymax,_stat)
				_matrix1 = _histo_object.statistic
			if self.gas: # gas components handled both by Pynbody and by YT
			# the reason is that Pynbody does not provide some hydro-variables, e.g. AMR level, or does not work well with some (e.g. temperature), while YT yields weird results for some (e.g. vrxy, vphi). The projected density is handled well by either, but I'm using YT for backwards compatibility
				_filen, _ = list(self.snaplist_dict.items())[0] # 1 item only
				if _xlabel[0] == 'x':
					if _ylabel[0] == 'y':
						projAxis=2
					else:
						projAxis=1
				else:
					projAxis=0
				if self.wvalue_var.get() == 'mass':
					_hydroVar_name = 'density'
				elif self.wvalue_var.get() == 'temp':
					_hydroVar_name = 'temperature'
				elif self.wvalue_var.get() == 'p':
					_hydroVar_name = 'pressure'
				elif self.wvalue_var.get() == 'level':
					_hydroVar_name = 'grid_level'
				else:
					messagebox.showwarning(title='Warning',
						message='Hydro variable {} not yet implemented!'.format(self.wvalue_var.get()))
				_matrix1 = \
					yt_utils.\
						weighted_map(_filen,_hydroVar_name,
						[_xmin,_xmax,_ymin,_ymax],
						projAxis,_resol)
			_cbmin = self.cbmin_var.get()
			_cbmax = self.cbmax_var.get()
			if self.cbscale_var.get() == 'log' or self.cbscale_var.get() == 'abs_log':
				_norm = mcolors.LogNorm(vmin=_cbmin,vmax=_cbmax)
				_tics_form = LogFormatterExponent(10,labelOnlyBase=False)
			else:
				_norm = mcolors.Normalize(vmin=_cbmin,vmax=_cbmax)
				_tics_form = None # i.e. default
			_extents = (_xmin,_xmax,_ymin,_ymax)
			# ensure square plots for equal axes ranges
			# if abs(_xmin) == abs(_xmax) and _xmin == _ymin \
			# 	and abs(_ymin) == abs(_ymax):
			# 		_aspect = 'equal'
			# else:
			_aspect = self.aspect_var.get()
				# _aspect = abs(_ymax - _ymin) / abs(_xmax - _xmin)
				# _aspect = 'auto'
			self._update_pb(win,self.progressbar_plot,50)
			self._update_pb(win,self.progressbar_plot,75)
			self._panel1.set_xlabel(_xlabel)
			self._panel1.set_ylabel(_ylabel)

			# smooth data
			_sigma = self.contour_sigma_var.get() # units?
			_levels = \
				tuple([float(i) for i in \
					self.contour_levels_var.get().split(',')])
			_linecolor = self.contour_color_var.get()
			_matrix1_smooth = gaussian_filter(_matrix1,_sigma)
			_ctr = self._panel1.contour(_matrix1_smooth,
				levels=_levels,
				norm=_norm,
				origin='lower',
				extent=_extents,
				colors=_linecolor)
			try:
				_legend = self.legend_var[0].get()
			except:
				_legend = os.path.basename(_filen)
			self._panel1.text(0.95,0.95,_legend,
				horizontalalignment='right',
				verticalalignment='top',
				transform=self._panel1.transAxes,
				bbox={'facecolor': 'white', 'pad': 4})
			self._panel1.set_aspect(_aspect)
			self.canvas.draw()
			self._update_pb(win,self.progressbar_plot,100)
			self.canvas_is_free = False


	def _save_data(self):
		'''
		Save (x,y) plot data to ascii (radial profiles) or binary (maps)
		'''
		if len(self.snaplist_dict) < 1:
			messagebox.showwarning(title='Warning',
				message='No snapshot yet loaded')
		# elif self.options_var.get() != 'generic_radial_profile':
		# 	messagebox.showwarning(message='Can only save radial profile data!')
		elif (self.xytable is None and self.xymap is None) or self.canvas_is_free:
			messagebox.showwarning(title='Warning',message='Cannot save empty data buffer!')
		else:
			if self.xytable is not None:
				_suggested_filename='rave_core_table.txt'
				filename = \
					filedialog.asksaveasfilename(initialfile=\
						_suggested_filename)
				if len(filename) > 5:
					rave_core.np.savetxt(filename,self.xytable)
					messagebox.showinfo(title=self._gui_title,
						message='Data saved to file {}'.format(filename))
				elif len(filename) != 0:
					messagebox.showwarning(title='GUI Graph Warning',
						message='File name too short: {}'.format(filename))
				else:
					pass
			if self.xymap is not None:
				_suggested_filename='rave_core_map.npy'
				filename = \
					filedialog.asksaveasfilename(initialfile=\
						_suggested_filename)
				if len(filename) > 5:
					rave_core.np.save(filename,self.xymap)
					messagebox.showinfo(title=self._gui_title,
						message='Data saved to file {}'.format(filename))
				elif len(filename) != 0:
					messagebox.showwarning(title='GUI Graph Warning',
						message='File name too short: {}'.format(filename))
				else:
					pass


	def _save_plot(self):
		'''
		Save plot to file (PNG, PDF, JPG, ...)
		'''
		if len(self.snaplist_dict) < 1:
			messagebox.showwarning(title='Warning',
				message='No snapshot yet loaded')
		elif self.canvas_is_free:
			messagebox.showwarning(message='Cannot save empty plot!')
		else:
			_suggested_filename='rave_core_plot.png'
			filename = \
				filedialog.asksaveasfilename(initialfile=_suggested_filename)
			if len(filename) > 5:
				self.figure.savefig(fname=filename,
					bbox_inches='tight')
				messagebox.showinfo(title=self._gui_title,
					message='Plot saved to file {}'.format(filename))
			elif len(filename) != 0:
				messagebox.showwarning(title='GUI Graph Warning',
					message='File name too short: {}'.format(filename))
			else:
				pass


	def _clear_plot(self,win=None):
		'''
		Fully resets plot canvas
		'''
		if self.options_var.get() == 'generic_radial_profile':
			self._reset_yrange_fields(1) # reset ymin/max values
		self._update_pb(win,self.progressbar_plot,0)
		self.figure.clear()
		self._panel1 = self.figure.add_subplot(1,1,1)
		self.canvas.draw() # very important!
		self.canvas_is_free = True
		self.xytable = None # ascii (tabular) plot data
		self.xymap = None	# binary (matrix) map / countour data
		win.update()




##############################################################################
def main():
	# create top-level window
	root = tk.Tk()
	# Instantiate class (no need to assig to an object)
	Pynbody_Ramses_GUI(root)
	# display window
	root.mainloop()


if __name__ == '__main__':

	# The following statemen is required to deter the standalone to open multiple GUI windows; it MUST be right after the 'if __name__' statement:
	# See:
	# - https://github.com/pyinstaller/pyinstaller/issues/2322
	# - https://github.com/mherrmann/fbs/issues/87#issuecomment-471489980
	# - https://github.com/pyinstaller/pyinstaller/wiki/Recipe-Multiprocessing
	# - https://stackoverflow.com/questions/32672596/pyinstaller-loads-script-multiple-times
	multiprocessing.freeze_support()

	# Invoke main function
	main()

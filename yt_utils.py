'''
========================================================================
AUTHOR:
    Dr. Thor Tepper Garcia

CREATED:
    02/02/2021
    -> In continuous development!

ABOUT:
    An class to provide a *customised* YT library (*) approach to the analysis of hydrodynamical simulations. Intended to be used in conjunction with a GUI interface, e.g. rave.py.

    (*) See:
        https://yt-project.org/doc/visualizing/plots.html#projection-plots
        https://yt-project.org/doc/reference/api/yt.data_objects.construction_data_containers.html#yt.data_objects.construction_data_containers.YTQuadTreeProj
========================================================================
'''


# import yt -> don't really need the entire library!
# from yt import load, visualization
from yt import load
from yt.visualization import fixed_resolution
import numpy as np
# from scipy.ndimage.filters import gaussian_filter
import math

# Whether to consider particles in addition to gas to locate the center of mass
# setting it to False sometimes yields incorrect results (e.g. fgas10/snap 162)
import sys
base_dir = '../GUI_tools/'
sys.path.insert(0, base_dir)
# the following becomes relevant when exporting the tools to other systems:
sys.path.insert(0, '../rave')

from particle_filter_yt import _filter_part_by_ID # custom function

_use_gas = False
_use_particles = True

def weighted_map(fileName,hydroVar,plotRange,projAxis,imgSize,compID_CoM=None,MaxCompNumber=11):
    '''
        Create a projection into a FixedResolutionBuffer and return as matrix,
        ready to plot by, e.g. matplotlib's imshow()
    '''
    if fileName[-1] == '/': fileName = fileName[:-1] # remove trailing /
    snap_str = fileName[-5:]
    # fileName += "/info_"+snap_str+".txt"

    ds = load(fileName)

    # create particle filter by ID (experimental as of 27 MAR 2023)
    # will be used to centre snapshot (akin to the -compID convention adopted in plotgal_multi.py):
    if compID_CoM is None: #or MaxCompNumber is None:
        new_part_type_str = 'nbody'
    else:
        try:
            new_part_type_str = _filter_part_by_ID(ds, compID_CoM, MaxCompNumber)
        except:
            print(f'WARNING: Cannot find particles with ID {compID_CoM}!')
            new_part_type_str = 'nbody'

    print(f'Using {new_part_type_str} to calculate centre of mass')

    # retrieve domain size (code units)
    dsize_cu = np.array(ds.domain_width)

    # retrieve all data
    # ad = ds.all_data()

    # apply spatial cuts
    # Explanation: Ramses output may contain particles at (or even beyond) the box edges (e.g. x|y|z =< 0 or x|y|z >= boxlen), and YT seems to include those at x|y|z =< 0 but ignore x|y|z >= boxlen, which introduces a bias in the position of the CoM; therefore, the solution is to remove all the particles at / beyond the box boundaries using include_inside(), which selects values inside the closed interval [min, max]
    # IMPORTANT: Do NOT use include_inside() -> FAILS!!!!
    # Rather, use 3D objects; see:
    # https://yt-project.org/doc/analyzing/objects.html#available-objects
    # ad = ds.r[:] # -> selects all data
    xyz_min = 1e-2 # code units; avoid boundary, i.e. 0.0
    xyz_max = 99e-2 # code units; avoid boundary, i.e. 1.0
    ad = ds.r[xyz_min:xyz_max] # -> selects data within region along 3D (*)
    # (*) no need to call all_data()

    dcenterOfMass = \
        ad.quantities.center_of_mass(use_gas=_use_gas, use_particles=_use_particles,
        particle_type=new_part_type_str).in_units('kpc')
    dcenterOfVelocity = \
        ad.quantities.bulk_velocity(use_gas=_use_gas, use_particles=_use_particles,
        particle_type=new_part_type_str).in_units('km/s')

    print(f'\tCentre of mass (position, kpc): {dcenterOfMass}')
    print(f'\tCentre of mass (velocity, km/s): {dcenterOfVelocity}')

    # get domain dimensions in kpc (but use np to strip units)
    dsize_kpc = np.array(ds.domain_width.in_units('kpc'))
    # dcenter_cu = np.array(ds.domain_center)
    dcenter_cu = np.array(dcenterOfMass  / dsize_kpc )
    # print(dcenter_cu)

    # note that bounds are wrt to the centre of mass!
    if projAxis == 0:
        xmin_cu = dcenter_cu[1] + plotRange[0]/dsize_kpc[1]
        xmax_cu = dcenter_cu[1] + plotRange[1]/dsize_kpc[1]
        ymin_cu = dcenter_cu[2] + plotRange[2]/dsize_kpc[2]
        ymax_cu = dcenter_cu[2] + plotRange[3]/dsize_kpc[2]
    if projAxis == 1:
        # this is NOT a right-hand projection
        xmin_cu = dcenter_cu[2] + plotRange[2]/dsize_kpc[0]
        xmax_cu = dcenter_cu[2] + plotRange[3]/dsize_kpc[0]
        ymin_cu = dcenter_cu[0] + plotRange[0]/dsize_kpc[2]
        ymax_cu = dcenter_cu[0] + plotRange[1]/dsize_kpc[2]
    if projAxis == 2:
        xmin_cu = dcenter_cu[0] + plotRange[0]/dsize_kpc[0]
        xmax_cu = dcenter_cu[0] + plotRange[1]/dsize_kpc[0]
        ymin_cu = dcenter_cu[1] + plotRange[2]/dsize_kpc[1]
        ymax_cu = dcenter_cu[1] + plotRange[3]/dsize_kpc[1]

    # image bounds
    bounds=(xmin_cu,xmax_cu,ymin_cu,ymax_cu) # in code units
    # print(bounds)

    # aspect ratio (an attempt to keep pixels square)
    plotRangeX = abs(xmax_cu - xmin_cu)
    plotRangeY = abs(ymax_cu - ymin_cu)
    if plotRangeY < plotRangeX:
        aspect = plotRangeY/plotRangeX
        imgSizeX = imgSize
        imgSizeY = math.ceil(aspect * imgSizeX)
    else:
        aspect = plotRangeX/plotRangeY
        imgSizeY = imgSize
        imgSizeX = math.ceil(aspect * imgSizeY)

    # create a projection
    if hydroVar == 'density':   # special case
        gas_dens_proj = \
            ds.proj( ('gas', hydroVar),
                axis=projAxis,
                max_level=ds.max_level,
                method='integrate')
    elif hydroVar == 'grid_level':  # special case
        gas_dens_proj = \
            ds.proj( ('index', hydroVar),
                axis=projAxis,
                max_level=ds.max_level,
                method='mip') # maximum along sightline
    else: # all other density weighted quantities
        gas_dens_proj = \
            ds.proj( ('gas', hydroVar),
                axis=projAxis,
                max_level=ds.max_level,
                method='integrate',
                weight_field='density')

    # create image buffer, i.e. a NxM matrix generator
    # here N=M, but they can be different
    # frb_proj = visualization.\
    #     fixed_resolution.\
    frb_proj = fixed_resolution.\
        FixedResolutionBuffer(gas_dens_proj,
            bounds,
            (imgSizeX, imgSizeY),
            antialias=True)

    # unit conversion and map shift
    map_shift = 0.
    if hydroVar == 'density':
        frb_proj.set_unit( ('gas', hydroVar), 'Msun/kpc**2')
    elif np.isin(hydroVar, list('xyz')):
        frb_proj.set_unit( ('gas', hydroVar), 'kpc')
        map_shift = -1.*dcenterOfMass[projAxis]
    elif hydroVar.split('_')[0] == 'velocity':
        frb_proj.set_unit( ('gas', hydroVar), 'km/s')
        map_shift =  -1.*dcenterOfVelocity[projAxis]
    elif hydroVar == 'grid_level':
        map_shift = ds.parameters['levelmin']
    else:
        pass

    # smooth (yt built-in)
    # _sigma = 1.5 # kpc or pixel?
    # if projAxis < 2:
    #     frb_proj.apply_gauss_beam(nbeam=10, sigma=_sigma)

    # retrieve map
    # NB: hydroVar alone as an argument of frb_proj does not work:
    if hydroVar == 'grid_level':
        matrix = frb_proj[ ('index', hydroVar) ]
    else:
        matrix = frb_proj[ ('gas', hydroVar) ]

    # add generic offset level (see above)
    matrix +=  map_shift

    if projAxis == 1:
        matrix = matrix.T #see (*) below

    # smooth (scipy)
    # _sigma = 1.5 # kpc or pixel?
    # matrix = gaussian_filter(matrix, _sigma)

    return matrix


# (*) From https://yt-project.org/doc/visualizing/plots.html#projection-plots :
# Flipping the plot view axes
# By default, all PlotWindow objects plot with the assumption that the eastern direction on the plot forms a right handed coordinate system with the normal and north_vector for the system, whether explicitly or implicitly defined. This setting can be toggled or explicitly defined by the user at initialization.
